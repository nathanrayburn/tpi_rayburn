<?php
/**
 * Author : Nathan Rayburn
 * Date: 3/18/21
 * Description : This file is designed to be the user management
 */

/**
 * This function is designed to get password from data base from the entered e-mail
 * @param $userUsername
 * @param $userPassword
 * @return bool, true if the data password is the same as the password that the user has entered otherwise false
 */
function isLoginCorrect($userUsername, $userPassword): bool
{
    //Set local variables
    $isLoginCorrect = false;
    $queryResult = null;
    $strSep = '\''; //string separator

   $loginQuery = 'SELECT password FROM users WHERE username = '.$strSep.$userUsername.$strSep; //set query

    require 'model/dbConnector.php';
    try{
        $queryResult = executeQuery($loginQuery);
        if(count($queryResult) == 1)
        {
            $userHashedPassword = $queryResult[0]["password"];
            if($userHashedPassword == $userPassword){
                $isLoginCorrect = true; //login succeeded
            }
        }
    }catch(Exception $exception)
    {
        //User doesn't exists
    }

    return $isLoginCorrect; //return bool
}

/**
 * This function is designed to check if the e-mail already exists in data base and then insert if not existant
 * @param $userUsername
 * @param $userPassword
 * @param $userEmail
 * @param $userFirstname
 * @param $userLastname
 * @return bool, true if no passwords has been returned by the data base with the entered e-mail by user otherwise false
 */
function isRegisterCorrect($userUsername,$userPassword,$userEmail,$userFirstname,$userLastname): bool
{
    //set local variables
    $registerCorrect = false;
    $strSep = '\'';
    $userHashedPassword = hash("sha256", $userPassword); //hash password
    //set query
    $query = "SELECT password FROM users WHERE username = ".$strSep.$userUsername.$strSep. "OR email =".$strSep.$userEmail.$strSep;

    require_once "model/dbConnector.php";
    try{

        $queryResult = executeQuery($query); //execute query

        if(count($queryResult) == 0) //check if user exists
        {
            $type = "user";
            //set query
            $insertionQuery="INSERT INTO users(username,password,email,name,lastname,type) VALUES('".$userUsername."','".$userHashedPassword."','".$userEmail."','".$userFirstname."','".$userLastname."','".$type."');";
            executeQueryWithNoReturn($insertionQuery); //execute query
            $registerCorrect=true;//execution succeeded

        }
    }catch(Exception $exception)
    {
        //E-mail or username already exists
    }

    return $registerCorrect; //return bool
}

/**
 * This funciton is designed to get the user's type from the data base
 * @param $userUsername string
 * @return array
 */
function getUserTypeFromDB($userUsername): ?array
{
    //set local variables
    $strSep = '\'';
    $type = "";
    //set query
    $query = "SELECT type FROM users WHERE username = ".$strSep.$userUsername.$strSep;
    require_once "model/dbConnector.php";
    try{
        $type = executeQuery($query);
    }catch(Exception $exception)
    {
        //User doesn't exist
    }
    return $type;
}

/**
 * This function is designed to get the user's personal information
 * @param $userUsername
 * @return array|null returns an array mixed of the user's personal information
 */
function getUserInfo($userUsername): ?array
{
    //set local variables
    $strSep = '\'';
    $info = null;
    //set query
    $query = $uniqueQuery = "SELECT * FROM users WHERE username = ".$strSep.$userUsername.$strSep;
    require_once "model/dbConnector.php";
    try{
        $info = executeQuery($query); //execute query
    }catch(Exception $exception)
    {
        //Couldn't get user's information
    }
    return $info;
}

/**
 * This function is designed to create the class for a user and set it to the SESSION
 * This function returns a bool if the user exists in the arguments
 * @param $results array string
 * @param $who string differences between a selected user and the user himself
 * @return bool returns true when class has been created for user successfully
 */
function createClass($results,$who):bool
{
    //set local variable
    $ok = false;

    require "class/userClass.php";

    if(count($results) == 1) //check if user array exists
    {
        $user = new User(); //new instance of a user
        $user->set_email($results[0]["email"]); //set email
        $user->set_firstname($results[0]["name"]); //set firstname
        $user->set_lastname($results[0]["lastname"]); //set lastname
        $user->set_username($results[0]["username"]); //set username
        if($who == "current")
        {
            $_SESSION["userInformation"] = serialize($user);
        }elseif ($who == "selected")
        {
            $_SESSION["userSelectedInformation"] = serialize($user);
        }

        $ok = true;
    }else{
        $ok = false;
    }
    return $ok;
}
/**
 * This function is designed to update the user's profile to the data base
 * @param $email string
 * @param $firstname string
 * @param $lastname string
 * @param $username string
 * @return bool returns true wehn the profile has been updated successfully
 */
function isProfileUpdateCorrect($email,$firstname,$lastname,$username): bool
{
    //set local variables
    $registerCorrect = false;
    $strSep = '\'';
    //set query
    $updateQuery = "UPDATE users SET name =".$strSep.$firstname.$strSep.", lastname =".$strSep.$lastname.$strSep.",email =".$strSep.$email.$strSep." WHERE username =".$strSep.$username.$strSep;
    require "model/dbConnector.php";
    try{ //update data base
        executeQueryWithNoReturn($updateQuery); //execute query
        $registerCorrect = true; //execution succeeded
    }catch(Exception $exception)
    {
        $registerCorrect = false;
        //Couldn't update the user's profile in the data base
    }
    return $registerCorrect; //return bool
}

/**
 * This function is designed to check if the current password is the same
 * @param $password string
 * @param $userUsername string
 * @return bool returns true when the user's password matches his user in the data base
 */
function isCurrentPasswordCorrect($password,$userUsername): bool
{
    //check if password is the same
    $passwordSame = false;
    $strSep = '\'';
    $userPassword = hash("sha256", $password);
    //set query
    $query = "SELECT password FROM users WHERE username = ".$strSep.$userUsername.$strSep;

    require "model/dbConnector.php";

    try{

        $result = executeQuery($query); //execute query
        if($result[0]["password"] == $userPassword) //check if passwords are the same
        {
            $passwordSame = true; //passwords are the same
        }

    }catch(Exception $exception)
    {
        //Couldn't get user's password
    }

    return $passwordSame; //return bool
}

/**
 * This function is designed to check if both new passwords are the same
 * @param $newPassword string
 * @param $repeatedPassword string
 * @return bool returns true when the repeated password is the same as the new password
 */
function isNewPasswordSame($newPassword,$repeatedPassword): bool
{
    //set local variables
    $isPasswordSame = false;
    $userPassword = hash("sha256", $newPassword);
    $userRepeat = hash("sha256",$repeatedPassword);

    if($userPassword == $userRepeat)     //check if both passwords are the same
    {
        $isPasswordSame = true;
    }

    return $isPasswordSame; //return bool
}

/**
 * This function is designed to get all accounts by a type (admin or user) from the data base
 * @param $userType string
 * @return array|null array type mixed returns a list of users
 */
function getAllUsersInfo($userType): ?array
{
    //set local variable
    $strSep = '\'';
    $results = null;
    //set query
    $query = "SELECT username FROM users WHERE type = ".$strSep.$userType.$strSep;

    require_once "model/dbConnector.php";

    try{
        $results = executeQuery($query);//executed query
    }catch(Exception $exception)
    {
        //couldn't retrieve all information from the selected user
    }
    return $results; //returns a list of users
}

/**
 * This function is designed to create instances of Users for all users
 * @param $listOfUsers array string
 * @return array of type user
 */
function createClassesForAllUsers($listOfUsers): array
{
    //set variables
    $listOfObjectTypeUsers = [];
    require "class/userClass.php";
    //create an instance of an object type user for each user in a list
    foreach($listOfUsers as $user)
    {
        $object = new User();
        $object->set_username($user["username"]);
        array_push($listOfObjectTypeUsers,$object);
    }

    return $listOfObjectTypeUsers; //return a list of objects type user
}

/**
 * This function is designed to update the user's information into the data base with the admin
 * @param $firstname string
 * @param $lastname string
 * @param $email string
 * @param $password string
 * @param $username string
 * @return bool returns true when the admin updates the user's personal information
 */
function isUpdateUserInformationAdmin($firstname,$lastname,$email,$password,$username): bool
{
    //set local variables
    $updateCorrect = false;
    $userPassword = hash("sha256", $password);
    $strSep = '\'';

    require_once "model/dbConnector.php";

        try{
            //update the data base
            $updateQuery = "UPDATE users SET name =".$strSep.$firstname.$strSep.", lastname =".$strSep.$lastname.$strSep.",email =".$strSep.$email.$strSep.",password =".$strSep.$userPassword.$strSep." WHERE username =".$strSep.$username.$strSep;
            executeQueryWithNoReturn($updateQuery); //execute query
            $updateCorrect = true; //execution succeeded
        }catch(Exception $exception)
        {
            //Couldn't update user's personal information
        }

    return $updateCorrect; //return bool
}

/**
 * This function is designed to update the user's password and returns a bool if it was successful
 * @param $username string
 * @param $password string
 * @return bool returns true when the password has been updated to the data base
 */
function isUpdateUserPassword($username, $password): bool
{
    //set local variable
    $updateCorrect = false;
    $userPassword = hash("sha256", $password);
    $strSep = '\'';
    //update the data base
    $updateQuery = "UPDATE users SET password =".$strSep.$userPassword.$strSep." WHERE username =".$strSep.$username.$strSep;

    try{
        executeQueryWithNoReturn($updateQuery); //execute query
        $updateCorrect = true; //execution successfully
    }catch(Exception $exception)
    {
        //Couldn't update the user's password
    }

    return $updateCorrect; //return bool
}

/**
 * This function is designed to delete a user from the data base
 * @param $username string
 * @return bool returns when the user has been deleted successfully from the data base
 */
function isDeleteUserCorrect(string $username):bool
{
    $IsDeleteUserCorrect = false;
    $strSep = '\'';
    try{
        $deleteQuery = "DELETE FROM users WHERE username =".$strSep.$username.$strSep;
        require_once "model/dbConnector.php";
        executeQueryWithNoReturn($deleteQuery);
        $IsDeleteUserCorrect = true;
    }catch(Exception $exception)
    {
        //Couldn't delete the selected user from data base
    }
    return $IsDeleteUserCorrect;
}
