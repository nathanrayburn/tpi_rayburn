<?php
/**
 * Author : Nathan Rayburn
 * Date: 31/05/21
 * Description : This file is designed to manage image upload to the server
 **/
/**
 * This function is designed to create a unique file name to prepare it for the data base
 * @param $username string
 * @param $category string
 * @return string returns a filename
 */
function createUniqueFileName($username,$category): string
{
    $originalFile = basename($_FILES["imgUpload"]["name"]); //get basename
    $imageFileType = strtolower(pathinfo($originalFile,PATHINFO_EXTENSION)); //get extension
    return $username . "-" . $category . "-" . date("Y-n-d-h-i-s").".".$imageFileType; //write filename
}

/**
 * This function is designed to upload the file to the server
 * In param. the file name for the upload
 * The function returns a bool if the upload has worked properly
 * @param $fileName string file's future name
 * @return bool returns true when there is an error
 */
function isUploadImage($fileName): bool
{
    //initialize local variables
    $target_dir = "img/";
    $originalFile = basename($_FILES["imgUpload"]["name"]);
    $uploadOk = false;

    $imageFileType = strtolower(pathinfo($originalFile,PATHINFO_EXTENSION));
    $target_file = $target_dir . $fileName;
    // Check if image file is a actual image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = false;
        } else {
            $uploadOk = true;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $uploadOk = true;
    }
    // Check file size
    if ($_FILES["imgUpload"]["size"] > 500000) {
        $uploadOk = true;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        $uploadOk = true;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == true) {
    } else {
        //upload file to server
        if (move_uploaded_file($_FILES["imgUpload"]["tmp_name"], $target_file)) {
            $uploadOk = false;
        } else {
            $uploadOk = true;
        }
    }
    return $uploadOk;
}