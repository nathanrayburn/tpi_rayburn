<?php
/**
 * Author : Nathan Rayburn
 * Date : 3/18/31
 * Description : This file is used to connect to data base and execute queries with or without returns
 */

/**
 * This function is designed to execute queries into data base and returns fetches the data selected
 * @param $query string
 * @return array|null returns an arraay of data from the data base
 */
function executeQuery($query): ?array
{
    $queryResult = null;

    $dbConnexion = openDBConnexion();

    if($dbConnexion != null)
    {
        $statement = $dbConnexion->prepare($query);
        $statement->execute();
        $queryResult = $statement->fetchAll();
    }
    $dbConnexion = null;
    return $queryResult;
}
/**
 * This function is designed to insert data into data base without returning data
 * Param query is the query used to execute the MYSQL command
 * @param $query string
 */
function executeQueryWithNoReturn($query)
{

    $dbConnexion = openDBConnexion();

    if($dbConnexion != null)
    {
        $statement = $dbConnexion->prepare($query);
        $statement->execute();
    }
    $dbConnexion = null;
}

/**
 * This function is designed to use the PDO driver to connect to data base
 * @return PDO|int
 */
function openDBConnexion(): PDO|int
{
    $dbConnexion = 0;

    $sqlDriver = 'mysql';
    $dbHostname = 'web22.swisscenter.com';
    $dbPort = '3306';
    $dbCharset = 'utf8';
    $dbName = 'tpi_nrn_admin';
    $dbUsername = 'tpi_nrn_admin';
    $dbPassword = 'Y3P.Vamr';

    $dbInfo = $sqlDriver.":host=".$dbHostname.";dbname=".$dbName.";port=".$dbPort.";charset=".$dbCharset;

    try
    {
        $_POST["error"] = 0;
        $dbConnexion = new PDO($dbInfo, $dbUsername, $dbPassword);
    }
    catch (PDOException $exception)
    {
        echo "Connexion failed; ".$exception->getMessage();
        $_POST["error"] = 1;
    }

    return $dbConnexion;
}










