<?php
/**
 * Author : Nathan Rayburn
 * Date: 18/05/21
 * Description : This file is designed to manage the inventory of components
 */

/**
 * This funciton is designed to change the public visibility state of a selected component
 * @param $category string
 * @param $id int
 * @return bool returns true when the component's state has been updated successfully
 */
function isChangedVisibilityState($category,$id): bool
{
    //Set local variables
    $isChangedVisibilityState = false;
    $results = null;
    $query = null;

    $component = getComponentInfo($category,$id); //get the component's data

    $state =(int)$component[0]["public_visibility"]; //change string to int and set the public visibility to a variable

    require_once "model/dbConnector.php";

    //switch the public visibility state
    if($state == 1)
    {
        $state =0;
    }else{
        $state = 1;
    }

    //Update the public visibility for the selected component
    switch($category){
        case "cases":
            $query = "UPDATE cases SET public_visibility =".$state." WHERE idcases =".$id;
            break;
        case "processors":
            $query = "UPDATE processors SET public_visibility =".$state." WHERE idprocessors =".$id;
            break;
        case "coolers":
            $query = "UPDATE coolers SET public_visibility =".$state." WHERE idcoolers =".$id;
            break;
        case "motherboard":
            $query = "UPDATE motherboard SET public_visibility =".$state." WHERE idmotherboard =".$id;
            break;
        case "graphics_cards":
            $query = "UPDATE graphics_cards SET public_visibility =".$state." WHERE idgraphics_cards =".$id;
            break;
        case "ram":
            $query = "UPDATE ram SET public_visibility =".$state." WHERE idram =".$id;
            break;
        case "power_supply":
            $query = "UPDATE power_supply SET public_visibility =".$state." WHERE idpower_supply =".$id;
            break;
        case "storage":
            $query = "UPDATE storage SET public_visibility =".$state." WHERE idstorage =".$id;
            break;
        default :
            return $isChangedVisibilityState = false;
    }
    try{
        executeQueryWithNoReturn($query); //execute query
    }catch(Exception $exception)
    {
        //Couldn't update visibility of the selected component
    }
    return $isChangedVisibilityState; //return bool
}

/**
 * This function is designed to get the components from a category
 * The function returns a list of data from the data base from the selected component category
 * @param $category string
 * @return array|null returns a list of all components from a category
 */
function inventoryComponentListAdmin($category): ?array
{
    //set local variables
    $results = null;
    $query = null;

    require_once "model/dbConnector.php";
    //set query in function of categories
    switch($category){
        case "cases":
            $query = "SELECT * FROM cases";
            break;
        case "processors":
            $query = "SELECT * FROM processors";
            break;
        case "coolers":
            $query = "SELECT * FROM coolers";
            break;
        case "motherboard":
            $query = "SELECT * FROM motherboard";
            break;
        case "graphics_cards":
            $query = "SELECT * FROM graphics_cards";
            break;
        case "ram":
            $query = "SELECT * FROM ram";
            break;
        case "power_supply":
            $query = "SELECT * FROM power_supply";
            break;
        case "storage":
            $query = "SELECT * FROM storage";
            break;
        default :
            $query = "SELECT * FROM cases";
            break;
    }
    try{
        $results = executeQuery($query);
    }catch(Exception $exception)
    {
        //Couldn't retrieve information from the selected component
    }
    return $results;
}

/**
 * This function is designed to get the components from a category
 * The function returns a list of data from the data base from the selected component category
 * @param $category string
 * @return array|null returns a list of components that are visible publicly for users from a category
 */
function inventoryComponentList($category): ?array
{
    //set local variables
    $results = null;
    $query = null;

    require_once "model/dbConnector.php";

    //set query in function of the category
    switch($category){
        case "cases":
            $query = "SELECT * FROM cases WHERE public_visibility = 1";
            break;
        case "processors":
            $query = "SELECT * FROM processors WHERE public_visibility = 1";
            break;
        case "coolers":
            $query = "SELECT * FROM coolers WHERE public_visibility = 1";
            break;
        case "motherboard":
            $query = "SELECT * FROM motherboard WHERE public_visibility = 1";
            break;
        case "graphics_cards":
            $query = "SELECT * FROM graphics_cards WHERE public_visibility = 1";
            break;
        case "ram":
            $query = "SELECT * FROM ram WHERE public_visibility = 1";
            break;
        case "power_supply":
            $query = "SELECT * FROM power_supply WHERE public_visibility = 1";
            break;
        case "storage":
            $query = "SELECT * FROM storage WHERE public_visibility = 1";
            break;
        default :
            $query = "SELECT * FROM cases WHERE public_visibility = 1";
            break;
    }
    try{
        $results = executeQuery($query);
    }catch(Exception $exception)
    {
        //Couldn't get components that are visible
    }
    return $results; //return array mixed from data base
}

/**
 * This function is designed to retrieve the column names from a table
 * @param $category string
 * @return array|null returns columns from a selected table
 */
function getColumnNamesFromTableByCategory($category): ?array
{
    $results = null;
    $query = null;
    require_once "model/dbConnector.php";
    switch($category){
        case "cases":
            $query = "SHOW COLUMNS FROM cases";
            break;
        case "processors":
            $query = "SHOW COLUMNS FROM processors";
            break;
        case "coolers":
            $query = "SHOW COLUMNS FROM coolers";
            break;
        case "motherboard":
            $query = "SHOW COLUMNS FROM motherboard";
            break;
        case "graphics_cards":
            $query = "SHOW COLUMNS FROM graphics_cards";
            break;
        case "ram":
            $query = "SHOW COLUMNS FROM ram";
            break;
        case "power_supply":
            $query = "SHOW COLUMNS FROM power_supply";
            break;
        case "storage":
            $query = "SHOW COLUMNS FROM storage";
            break;
    }
    try{
        $results = executeQuery($query);
    }catch(Exception $exception)
    {
        //Couldn't get columns
    }
    return $results;
}

/**
 * This function is designed to check if the selected item exists in the data base and returns a bool answer in function if it exists
 * @param $category string
 * @param $id int
 * @return bool returns true when the selected component exists
 */
function IsSelectedItemExists($category,$id)
{
    $IsSelectedItemExists = false;
    $strSep = '\'';
    $query = null;
    $results = null;
    require_once "model/dbConnector.php";
    switch($category){
        case "cases":
            $query = "SELECT * FROM cases where idcases = ".$strSep.$id.$strSep;
            break;
        case "processors":
            $query = "SELECT * FROM processors where idprocessors = ".$strSep.$id.$strSep;
            break;
        case "coolers":
            $query = "SELECT * FROM coolers where idcoolers = ".$strSep.$id.$strSep;
            break;
        case "motherboard":
            $query = "SELECT * FROM motherboard where idmotherboard = ".$strSep.$id.$strSep;
            break;
        case "graphics_cards":
            $query = "SELECT * FROM graphics_cards where idgraphics_cards = ".$strSep.$id.$strSep;
            break;
        case "ram":
            $query = "SELECT * FROM ram where idram = ".$strSep.$id.$strSep;
            break;
        case "power_supply":
            $query = "SELECT * FROM power_supply where idpower_supply = ".$strSep.$id.$strSep;
            break;
        case "storage":
            $query = "SELECT * FROM storage where idstorage = ".$strSep.$id.$strSep;
            break;
    }
    try{
        $results = executeQuery($query);
    }catch(Exception $exception)
    {
        //Couldn't get information about the selected component
    }
    if(count($results)== 1) //checks if the component exists
    {
        $IsSelectedItemExists = true;
    }
    return $IsSelectedItemExists; //return bool
}

/**
 * This function is designed to get all information concerning a selected component
 * @param $category string
 * @param $id int
 * @return array|null returns an array of data from a component
 */
function getComponentInfo($category,$id): ?array
{
    //set local variables
    $query = null;
    $results = null;

    require_once "model/dbConnector.php";
    //set query in function of the category
    switch($category){
        case "cases":
            $query = "SELECT * FROM cases where idcases = ".$id;
            break;
        case "processors":
            $query = "SELECT * FROM processors where idprocessors = ".$id;
            break;
        case "coolers":
            $query = "SELECT * FROM coolers where idcoolers = ".$id;
            break;
        case "motherboard":
            $query = "SELECT * FROM motherboard where idmotherboard = ".$id;
            break;
        case "graphics_cards":
            $query = "SELECT * FROM graphics_cards where idgraphics_cards = ".$id;
            break;
        case "ram":
            $query = "SELECT * FROM ram where idram = ".$id;
            break;
        case "power_supply":
            $query = "SELECT * FROM power_supply where idpower_supply = ".$id;
            break;
        case "storage":
            $query = "SELECT * FROM storage where idstorage = ".$id;
            break;
    }
    try{
        $results = executeQuery($query); //execute the query
    }catch(Exception $exception)
    {
        //Couldn't get any information from the selected component
    }

    return $results; //returns an array of a data of a component
}

/**
 * This function is designed to remove the basic attributes such as name or description so we can have only the properties of the component
 * @param $attributes
 * @return array returns a filtered list of properties of a table
 */
function sortProperties($attributes): array
{
    $propertiesList = []; //initialize local variable
        for($i=0;$i<count($attributes);$i++) //goes through list of attributes and removes default fields (description,name,price,image,public visibility)
        {
                if($attributes[$i]["Field"] != "name" && $attributes[$i]["Field"] != "description" && $attributes[$i]["Field"] != "image" && $attributes[$i]["Field"] != "price" && $attributes[$i]["Field"] != "public_visibility")
                {
                   array_push($propertiesList, $attributes[$i]);
                }
        }
        return $propertiesList; //return array type string
}

/**
 * This function is designed to create or update a new case into the data base
 * This function returns a bool if it has been the query has been executed correctly, it will return true
 * @param $id string
 * @param $name string
 * @param $description string
 * @param $price float
 * @param $image_path string
 * @param $max_height_cpu int
 * @param $max_length_gpu int
 * @param $form_factor_motherboard string
 * @param $form_factor_power_supply string
 * @param $aio bool
 * @return bool returns true when a case is created in the data base
 */
function isCreateCasesInDB($id,$name,$description,$price,$image_path,$max_height_cpu,$max_length_gpu,$form_factor_motherboard,$form_factor_power_supply,$aio): bool
{
    //set local variables
    $isCreateCasesInDB = false;
    $visibility = 1; //set visibility true by default
    $aio_water = 0; //set aio water cooler false by default
    $strSep = '"';
    $update = false;
    $id = (int)$id;
    if($aio == true) //set water cooler to 1 if it was selected in form
    {
        $aio_water = 1;
    }
    if($id != null && $id != 0)
    {
        $update = true;
    }
    if($update)
    {
        //set query if it's an component update
        $insertionQuery="UPDATE cases SET name =".$strSep.$name.$strSep.", description =".$strSep.$description.$strSep.",image =".$strSep.$image_path.$strSep.",price = ".$strSep.$price.$strSep.", form_factor_motherboard = ".$strSep.$form_factor_motherboard.$strSep.", max_height_cpu = ".$strSep.$max_height_cpu.$strSep.", max_length_gpu =".$strSep.$max_length_gpu.$strSep.",aio_water = ".$strSep.$aio_water.$strSep.", form_factor_power_supply = ".$strSep.$form_factor_power_supply.$strSep."  WHERE idcases =".$id;
    }else{
        //set query if it's a new component
        $insertionQuery="INSERT INTO cases(name,description,image,price,form_factor_motherboard,max_length_gpu,max_height_cpu,aio_water,public_visibility,form_factor_power_supply) VALUES(".$strSep.$name.$strSep.",".$strSep.$description.$strSep.",".$strSep.$image_path.$strSep.",".$strSep.$price.$strSep.",".$strSep.$form_factor_motherboard.$strSep.",".$max_length_gpu.",".$max_height_cpu.",".$aio_water.",".$visibility.",".$strSep.$form_factor_power_supply.$strSep.");";

    }

    require_once "model/dbConnector.php";

    try{
        executeQueryWithNoReturn($insertionQuery); //execute query
        $isCreateCasesInDB=true; //successfully executed

    }catch(Exception $exception){
        //couldn't create or update component in the data base
    }

    return $isCreateCasesInDB; //return bool
}

/**
 * This function is designed to create or update a new processor into the data base
 * This function returns a bool if it has been the query has been executed correctly, it will return true
 * @param $id string
 * @param $name string
 * @param $description string
 * @param $price float
 * @param $image_path string
 * @param $socket string
 * @return bool returns true when a processor is created in the data base
 */
function IsCreateProcessorInDB($id,$name,$description,$price,$image_path,$socket): bool
{
    //set local variables
    $IsCreateProcessorInDB = false;
    $visibility = 1;
    $strSep = '"';
    $update = false;
    $id = (int)$id;
    if($id != null && $id != 0)
    {
        $update = true;
    }
    //set query
    if($update)
    {
        $insertionQuery="UPDATE processors SET name =".$strSep.$name.$strSep.", description =".$strSep.$description.$strSep.",image =".$strSep.$image_path.$strSep.",price = ".$strSep.$price.$strSep.", socket_cpu = ".$strSep.$socket.$strSep." WHERE idprocessors =".$id;
    }else{
        $insertionQuery="INSERT INTO processors(name,description,image,price,socket_cpu,public_visibility) VALUES(".$strSep.$name.$strSep.",".$strSep.$description.$strSep.",".$strSep.$image_path.$strSep.",".$strSep.$price.$strSep.",".$strSep.$socket.$strSep.",".$strSep.$visibility.$strSep.");";
    }

    require_once "model/dbConnector.php";
    try{
        executeQueryWithNoReturn($insertionQuery); //execute query
        $IsCreateProcessorInDB=true; //execution succeeded

    }catch(Exception $exception){
        //couldn't create or update component in the data base
    }

    return $IsCreateProcessorInDB; //return bool
}

/**
 * This function is designed to create or update a new cooler into the data base
 * This function returns a bool if it has been the query has been executed correctly, it will return true
 * @param $id string
 * @param $name string
 * @param $description string
 * @param $price float
 * @param $image_path string
 * @param $socket string
 * @param $height integer
 * @param $aio bool
 * @return bool returns true when a cooler is created in the data base
 */
function IsCreateCoolersInDB($id,$name,$description,$price,$image_path,$socket,$height,$aio): bool
{
    //set local variables
    $IsCreateCoolersInDB = false;
    $visibility = 1;
    $strSep = '"';
    $update = false;
    $id = (int)$id;
    if($aio == true) //set water cooler to 1 if it was selected in form
    {
        $aio_water = 1;
    }
    if($id != null && $id != 0)
    {
        $update = true;
    }
    //set query
    if($update)
    {
        $insertionQuery="UPDATE coolers SET name =".$strSep.$name.$strSep.", description =".$strSep.$description.$strSep.",image =".$strSep.$image_path.$strSep.",price = ".$strSep.$price.$strSep.", socket_support = ".$strSep.$socket.$strSep.", max_height = ".$strSep.$height.$strSep.",aio_water = ".$strSep.$aio_water.$strSep." WHERE idcoolers =".$id;
    }else{
        $insertionQuery="INSERT INTO coolers(name,description,image,price,socket_support,max_height,aio_water,public_visibility) VALUES(".$strSep.$name.$strSep.",".$strSep.$description.$strSep.",".$strSep.$image_path.$strSep.",".$strSep.$price.$strSep.",".$strSep.$socket.$strSep.",".$strSep.$height.$strSep.",".$strSep.$aio.$strSep.",".$strSep.$visibility.$strSep.");";
    }

    require_once "model/dbConnector.php";
    try{
        executeQueryWithNoReturn($insertionQuery); //execute query
        $IsCreateCoolersInDB=true; //execution succeeded
    }catch(Exception $exception){
        //couldn't create or update component in the data base
    }

    return $IsCreateCoolersInDB; //return bool
}

/**
 * This function is designed to create or update a new motherboard into the data base
 * @param $id string
 * @param $name string
 * @param $description string
 * @param $price float
 * @param $image_path string
 * @param $socket string
 * @param $form_factor_ram string
 * @param $form_factor string
 * @param $interface_m2 string
 * @return bool returns true when a motherboard has been successfully created in the data base
 */
function IsCreateMotherboardInDB($id,$name,$description,$price,$image_path,$socket,$form_factor_ram,$form_factor,$interface_m2): bool
{
    //set local variables
    $IsCreateMotherboardInDB = false;
    $visibility = 1;
    $strSep = '"';
    $update = false;
    $id = (int)$id;
    if($id != null && $id != 0)
    {
        $update = true;
    }
    //set query
    if($update)
    {
        $insertionQuery="UPDATE motherboard SET name =".$strSep.$name.$strSep.", description =".$strSep.$description.$strSep.",image =".$strSep.$image_path.$strSep.",price = ".$strSep.$price.$strSep.", socket_cpu = ".$strSep.$socket.$strSep.",form_factor = ".$strSep.$form_factor.$strSep.",form_factor_ram = ".$strSep.$form_factor_ram.$strSep.",interface_m2 = ".$strSep.$interface_m2.$strSep." WHERE idmotherboard =".$id;
    }else{
        $insertionQuery="INSERT INTO motherboard(name,description,image,price,socket_cpu,form_factor,form_factor_ram,interface_m2,public_visibility) VALUES(".$strSep.$name.$strSep.",".$strSep.$description.$strSep.",".$strSep.$image_path.$strSep.",".$strSep.$price.$strSep.",".$strSep.$socket.$strSep.",".$strSep.$form_factor.$strSep.",".$strSep.$form_factor_ram.$strSep.",".$strSep.$interface_m2.$strSep.",".$strSep.$visibility.$strSep.");";
   }
    require_once "model/dbConnector.php";
    try{
        executeQueryWithNoReturn($insertionQuery); //execute query
        $IsCreateMotherboardInDB=true; //execution succeeded

    }catch(Exception $exception){
        //couldn't create or update component in the data base
    }

    return $IsCreateMotherboardInDB; //return bool
}

/**
 * This function is designed to create or update a new graphics card into the data base
 * This function returns a bool if it has been the query has been executed correctly, it will return true
 * @param $id string
 * @param $name string
 * @param $description string
 * @param $price float
 * @param $image_path integer
 * @param $max_length_gpu integer
 * @return bool returns true when a graphics card has been successfully created in the data base
 */
function IsCreateGraphicsCardInDB($id,$name,$description,$price,$image_path,$max_length_gpu): bool
{
    //set local variables
    $IsCreateGraphicsCardInDB = false;
    $visibility = 1;
    $strSep = '"';
    $update = false;
    $id = (int)$id;
    if($id != null && $id != 0)
    {
        $update = true;
    }
    //set query
    if($update)
    {
        $insertionQuery="UPDATE graphics_cards SET name =".$strSep.$name.$strSep.", description =".$strSep.$description.$strSep.",image =".$strSep.$image_path.$strSep.",price = ".$strSep.$price.$strSep.", max_length_gpu = ".$strSep.$max_length_gpu.$strSep." WHERE idgraphics_cards =".$id;
    }else{
        $insertionQuery="INSERT INTO graphics_cards(name,description,image,price,max_length_gpu,public_visibility) VALUES(".$strSep.$name.$strSep.",".$strSep.$description.$strSep.",".$strSep.$image_path.$strSep.",".$strSep.$price.$strSep.",".$strSep.$max_length_gpu.$strSep.",".$strSep.$visibility.$strSep.");";
   }

    require_once "model/dbConnector.php";

    try{

        executeQueryWithNoReturn($insertionQuery); //execute query
        $IsCreateGraphicsCardInDB=true; //execution succeeded

    }catch(Exception $exception){
        //couldn't create or update component in the data base
    }

    return $IsCreateGraphicsCardInDB; //return bool
}

/**
 * This function is designed to create or update a new graphics card into the data base
 * This function returns a bool if it has been the query has been executed correctly, it will return true
 * @param $id string
 * @param $name string
 * @param $description string
 * @param $price float
 * @param $image_path string
 * @param $form_factor string
 * @return bool returns true when ram has been successfully created in the data base
 */
function IsCreateRamInDB($id,$name,$description,$price,$image_path,$form_factor): bool
{
    //set local variables
    $IsCreateRamInDB = false;
    $visibility = 1;
    $strSep = '"';
    $update = false;
    $id = (int)$id;
    if($id != null && $id != 0)
    {
        $update = true;
    }
    //set query
    if($update)
    {
        $insertionQuery="UPDATE ram SET name =".$strSep.$name.$strSep.", description =".$strSep.$description.$strSep.",image =".$strSep.$image_path.$strSep.",price = ".$strSep.$price.$strSep.", form_factor = ".$strSep.$form_factor.$strSep." WHERE idram =".$id;
    }else{
        $insertionQuery="INSERT INTO ram(name,description,image,price,form_factor,public_visibility) VALUES(".$strSep.$name.$strSep.",".$strSep.$description.$strSep.",".$strSep.$image_path.$strSep.",".$strSep.$price.$strSep.",".$strSep.$form_factor.$strSep.",".$strSep.$visibility.$strSep.");";
    }

   require_once "model/dbConnector.php";
    try{
        executeQueryWithNoReturn($insertionQuery); //execute query
        $IsCreateRamInDB=true; //execution succeeded
    }catch(Exception $exception){
        //couldn't create or update component in the data base
    }

    return $IsCreateRamInDB; //return bool
}

/**
 * This function is designed to create or update a new power supply into the data base
 * This function returns a bool if it has been the query has been executed correctly, it will return true
 * @param $id string
 * @param $name string
 * @param $description string
 * @param $price float
 * @param $image_path string
 * @param $form_factor string
 * @return bool returns true when a power supply has been successfully created in the data base
 */
function IsCreatePowerSupplyInDB($id,$name,$description,$price,$image_path,$form_factor): bool
{
    //set local variables
    $IsCreatePowerSupplyInDB = false;
    $visibility = 1;
    $strSep = '"';
    $update = false;
    $id = (int)$id;
    if($id != null && $id != 0)
    {
        $update = true;
    }
    //set query
    if($update)
    {
        $insertionQuery="UPDATE power_supply SET name =".$strSep.$name.$strSep.", description =".$strSep.$description.$strSep.",image =".$strSep.$image_path.$strSep.",price = ".$strSep.$price.$strSep.", form_factor = ".$strSep.$form_factor.$strSep." WHERE idpower_supply =".$id;
    }else{
        $insertionQuery="INSERT INTO power_supply(name,description,image,price,form_factor,public_visibility) VALUES(".$strSep.$name.$strSep.",".$strSep.$description.$strSep.",".$strSep.$image_path.$strSep.",".$strSep.$price.$strSep.",".$strSep.$form_factor.$strSep.",".$strSep.$visibility.$strSep.");";
    }

    require_once "model/dbConnector.php";

    try{

        executeQueryWithNoReturn($insertionQuery); //execute query
        $IsCreatePowerSupplyInDB=true; //query succeeded

    }catch(Exception $exception){
        //couldn't create or update component in the data base
    }

    return $IsCreatePowerSupplyInDB; //return bool
}

/**
 * This function is designed to create or update a new storage device into the data base
 * This function returns a bool if it has been the query has been executed correctly, it will return true
 * @param $id string
 * @param $name string
 * @param $description string
 * @param $price float
 * @param $image_path string
 * @param $form_factor string
 * @param $interface_m2 string
 * @return bool returns true when a storage device has been successfully created in the data base
 */
function IsCreateStorageInDB($id,$name,$description,$price,$image_path,$form_factor,$interface_m2): bool
{
    //set local variables
    $IsCreateStorageInDB = false;
    $visibility = 1;
    $strSep = '"';
    $update = false;
    $id = (int)$id;
    if($id != null && $id != 0)
    {
        $update = true;
    }
    //set query
    if($update)
    {
        $insertionQuery="UPDATE storage SET name =".$strSep.$name.$strSep.", description =".$strSep.$description.$strSep.",image =".$strSep.$image_path.$strSep.",price = ".$strSep.$price.$strSep.", form_factor = ".$strSep.$form_factor.$strSep.",interface_m2 = ".$strSep.$interface_m2.$strSep." WHERE idstorage =".$id;
    }else{
        $insertionQuery="INSERT INTO storage(name,description,image,price,form_factor,interface_m2,public_visibility) VALUES(".$strSep.$name.$strSep.",".$strSep.$description.$strSep.",".$strSep.$image_path.$strSep.",".$strSep.$price.$strSep.",".$strSep.$form_factor.$strSep.",".$strSep.$interface_m2.$strSep.",".$strSep.$visibility.$strSep.");";
    }

    require_once "model/dbConnector.php";
    try{

        executeQueryWithNoReturn($insertionQuery); //execute query
        $IsCreateStorageInDB=true; //execution succeeded

    }catch(Exception $exception){

        //couldn't create or update component in the data base

    }
    return $IsCreateStorageInDB; //return bool
}

/**
 * This function is designed to split a string by a separator and then return a string with a space
 * @param $category string
 * @return string returns a formatted string
 */
function formatTitle($category): string
{
    $temp = explode("_",$category); //separate string into array of string
    return implode(" ",$temp); //return formatted string

}