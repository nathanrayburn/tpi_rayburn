<?php
/**
 * Author : Nathan Rayburn
 * Date : 31/05/21
 * Description : This file is designed to export a file to PDF from HTML
 */
use Mpdf\MpdfException;
/**
 * This function is designed to export to PDF from data in HTML format
 * @param $cases array
 * @param $coolers array
 * @param $processor array
 * @param $ram array
 * @param $graphics_cards array
 * @param $storage array
 * @param $power_supply array
 * @param $motherboard array
 * @throws MpdfException
 */
function exportHTMlToPDF($cases,$coolers,$processor,$ram,$graphics_cards,$storage,$power_supply,$motherboard)
{
    //initialize local variables
    $listOfComponents = [];
    $totalPrice = 0;

    $mpdf = new \Mpdf\Mpdf(); //Instance an object to make a PDF
    array_push($listOfComponents, $cases, $coolers, $processor, $ram, $graphics_cards, $storage, $power_supply, $motherboard); //add components in a list
    $mpdf->showImageErrors = true; //set image errors
    $stylesheet = file_get_contents('css/custom.css'); //copy custom css file

    $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS); //set custom css file
    $mpdf->WriteHTML('<h1>List of components</h1>');

    //write data from each selected part by user
    foreach ((array)$listOfComponents as $component) {
        //skip components that are null
        if ($component != null) {

            $mpdf->WriteHTML('<hr class="solid">'); //set separator

            $image = "img/defaultImage.jpg"; //set default image
            $totalPrice = $totalPrice + $component[0]['price']; //calculate price
            $type = false;
            if ($component[0]['image'] != "") { //set image in PDF if image exists
                $image = $component[0]['image']; //get image
                $type = true;
            }
            if ($type) { //write image if exists
                $mpdf->WriteHTML('<h2>Model ' . $component[0]['name'] . '</h2><img src="' . $image . '" style="width: 20mm; height: 20mm; margin: 0;" /><h3>Description</h3><p>' . $component[0]['description'] . '</p><h3>Price</h3><p>The costing price of the product is ' . $component[0]['price'] . ' CHF.</p>');
            } else {
                $mpdf->WriteHTML('<h2>Model ' . $component[0]['name'] . '</h2><h3>Description</h3><p>' . $component[0]['description'] . '</p><h3>Price</h3><p>The costing price of the product is ' . $component[0]['price'] . ' CHF.</p>');
            }
        }
    }
    $mpdf->WriteHTML('<hr class="solid">'); //write separator
    $mpdf->WriteHTML('<h3 class="">Total price</h3>');
    $mpdf->WriteHTML('<p class="">The total price of the components is ' . $totalPrice . ' CHF</p>'); //write total price
    $mpdf->Output(); //export to PDF
}