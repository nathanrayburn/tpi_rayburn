<?php
/**
 * Author : Nathan Rayburn
 * Date: 31/05/21
 * Description : This file is designed to create temp json files for a user
 **/
/**
 * This function is designed to create a temp json file
 * @param $username string
 * @param $value string
 */
function createTempFile($username,$value)
{
    $target_dir = "temp/";
    $temp = "temp";
    $strSep = '.';
    $ext = ".json";
    try{
        $fp = fopen($target_dir.$username.$strSep.$temp.$ext, 'w');
        fwrite($fp, $value);
        fclose($fp);
    }catch(Exception $exception)
    {
        echo "Couldn't create temp file";
    }
}