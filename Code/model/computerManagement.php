<?php
/**
 * Author : Nathan Rayburn
 * Date: 27/05/21
 * Description : This file is designed to manage the inventory of computers of any type
 **/

/**
 * This function is designed to create a new computer into the data base
 * @param $username string
 * @param $cases array
 * @param $coolers array
 * @param $processor array
 * @param $ram array
 * @param $graphics_cards array
 * @param $storage array
 * @param $power_supply array
 * @param $motherboard array
 * @param $type int
 * @return bool returns true when a computer has been created successfully
 */

function isCreateComputerInDB($username,$cases,$coolers,$processor,$ram,$graphics_cards,$storage,$power_supply,$motherboard,$image,$type):bool
{
    //format variables for SQL query
    $registrationDate = "'".date('Y-m-d H:i:s')."'";
    $image = "'".$image."'";
    $type = "'".$type."'";

    //initialize local variables
    $isCreateComputerInDB = false;
    $userInformation = null;
    $userID = null;
    $casesID = null;
    $coolersID = null;
    $processorID = null;
    $ramID = null;
    $graphics_cardsID = null;
    $storageID = null;
    $power_supplyID = null;
    $motherboardID = null;

    //Check if the ID of a component is null, if it is, local variable is set explicitly null so that the query will be readable by the data base
    if (!empty($ram[0][0])) {  //check null for the ram
        $ramID = "'".$ram[0][0]."'";
    } else {
        $ramID = "NULL";
    }
    if (!empty($graphics_cards[0][0])) { //check null for the graphics cards
        $graphics_cardsID = "'".$graphics_cards[0][0]."'";
    } else {
        $graphics_cardsID = "NULL";
    }
    if (!empty($storage[0][0])) { //check null for the storage
        $storageID = "'".$storage[0][0]."'";
    } else {
        $storageID = "NULL";
    }
    if (!empty($power_supply[0][0])) { //check null for the power supply
        $power_supplyID = "'".$power_supply[0][0]."'";
    } else {
        $power_supplyID = "NULL";
    }
    if (!empty($motherboard[0][0])) { //check null for the motherboard
        $motherboardID = "'".$motherboard[0][0]."'";
    } else {
        $motherboardID = "NULL";
    }
    if (!empty($processor[0][0])) { //check null for the motherboard
        $processorID = "'".$processor[0][0]."'";
    } else {
        $processorID = "NULL";
    }
    if (!empty($coolers[0][0])) { //check null for the cpu coolers
        $coolersID = "'".$coolers[0][0]."'";
    } else {
        $coolersID = "NULL";
    }
    if (!empty($cases[0][0])) { //check null for the cases
        $casesID = "'".$cases[0][0]."'";
    } else {
        $casesID = "NULL";
    }

    //get the user's ID to insert a computer under his ID
    try{
        require_once "model/userManagement.php";
        $userInformation = getUserInfo($username);

    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    $userID = $userInformation[0]["idusers"]; //set the user ID from the returned array
    //insert into the data base the computer
    try{
        $insertQuery = "INSERT INTO computers (example,image,registration_date,users_idusers,cases_idcases,motherboard_idmotherboard,processors_idprocessors,storage_idstorage,power_supply_idpower_supply,ram_idram,graphics_cards_idgraphics_cards,coolers_idcoolers) VALUES(".$type.','.$image.','.$registrationDate.','.$userID.','.$casesID.','.$motherboardID.','.$processorID.','.$storageID.','.$power_supplyID.','.$ramID.','.$graphics_cardsID.','.$coolersID.")";
        require_once "model/dbConnector.php";
        executeQueryWithNoReturn($insertQuery);
        $isCreateComputerInDB = true; //set true if no error has been created
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    return $isCreateComputerInDB;
}

/**
 * This function is designed to update a computer build that is already in the data base
 * @param $cases array
 * @param $coolers array
 * @param $processor array
 * @param $ram array
 * @param $graphics_cards array
 * @param $storage array
 * @param $power_supply array
 * @param $motherboard array
 * @param $image string
 * @param $id int
 * @return bool returns true when the computer has been updated successfully in the data base
 */
function isUpdateComputerInDB($username,$cases,$coolers,$processor,$ram,$graphics_cards,$storage,$power_supply,$motherboard,$image,$id): bool
{
    //set local variaables
    $strSep =     $strSep = '\'';

    $isUpdateComputerInDB = false;
    $userInformation = null;
    $userID = null;
    $casesID = null;
    $coolersID = null;
    $processorID = null;
    $ramID = null;
    $graphics_cardsID = null;
    $storageID = null;
    $power_supplyID = null;
    $motherboardID = null;
    $userInformation = null;

    $computerResult = [];
    $userBuildID = 0;
    $userBuildType = "";
    //format the string to be set in query
    $image = $strSep.$image.$strSep;

    if (!empty($ram[0][0])) { //check null for the ram
        $ramID = $strSep.$ram[0][0].$strSep;
    } else {
        $ramID = "NULL";
    }
    if (!empty($graphics_cards[0][0])) { //check null for the graphics cards
        $graphics_cardsID = $strSep.$graphics_cards[0][0].$strSep;
    } else {
        $graphics_cardsID = "NULL";
    }
    if (!empty($storage[0][0])) { //check null for the storage
        $storageID = $strSep.$storage[0][0].$strSep;
    } else {
        $storageID = "NULL";
    }
    if (!empty($power_supply[0][0])) { //check null for the power supply
        $power_supplyID = $strSep.$power_supply[0][0].$strSep;
    } else {
        $power_supplyID = "NULL";
    }
    if (!empty($motherboard[0][0])) { //check null for the motherboard
        $motherboardID = $strSep.$motherboard[0][0].$strSep;
    } else {
        $motherboardID = "NULL";
    }
    if (!empty($processor[0][0])) { //check null for the processor
        $processorID = $strSep.$processor[0][0].$strSep;
    } else {
        $processorID = "NULL";
    }
    if (!empty($coolers[0][0])) { //check null for the cpu coolers
        $coolersID = $strSep.$coolers[0][0].$strSep;
    } else {
        $coolersID = "NULL";
    }
    if (!empty($cases[0][0])) { //check null for the cases
        $casesID = $strSep.$cases[0][0].$strSep;
    } else {
        $casesID = "NULL";
    }
    //get the user's information
    try{

        require_once "model/userManagement.php";
        $userInformation = getUserInfo($username);

    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    $userID = $userInformation[0]['idusers']; //set ID
    $userType = $userInformation[0]['type']; //set type

    //get user id from build and build type (example or not)
    try{
        $query = "SELECT computers.users_idusers, computers.example FROM computers WHERE computers.idcomputers = ".$strSep.$id.$strSep;
        require_once "model/dbConnector.php";
        $userBuildID = executeQuery($query);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    //check if the user is admin and that computer that is being saved is an example
    if($userType == "admin" && (int)$userBuildID[0]['example'] == 1)
    {
        //get the user's type
        try{
            $query = "SELECT type FROM users WHERE idusers =".$strSep.$userBuildID[0][0].$strSep;
            require_once "model/dbConnector.php";
            $userBuildType = executeQuery($query);
        }catch(Exception $exception)
        {
            echo "Error : ".$exception;
        }
        //check if the user's type that built the computer corresponds is the same as current user
        if($userType == $userBuildType[0]['type'])
        {
            //get the computer's ID from data base
        try{
                $query = "SELECT idcomputers from computers where idcomputers =".$strSep.$id.$strSep." ";
                require_once "model/dbConnector.php";
                $computerResult = executeQuery($query);
            }catch(Exception $exception)
            {
                echo "Error : ".$exception;
            }
        }
    }else{
        try{
            $query = "SELECT idcomputers from computers where users_idusers =".$strSep.$userID.$strSep." AND idcomputers =".$strSep.$id.$strSep." ";
            require_once "model/dbConnector.php";
            $computerResult = executeQuery($query);
        }catch(Exception $exception)
        {
            echo "Error : ".$exception;
        }
    }

    try{
        //Check if the computer still exists in the data base
        if(count($computerResult) == 1)
        {
            //update the selected computer
            $updateQuery = "UPDATE computers SET image =".$image." , cases_idcases =".$casesID." , motherboard_idmotherboard = ".$motherboardID." , processors_idprocessors = ".$processorID.", storage_idstorage = ".$storageID." , power_supply_idpower_supply = ".$power_supplyID.", ram_idram = ".$ramID.", graphics_cards_idgraphics_cards = ".$graphics_cardsID.", coolers_idcoolers = ".$coolersID." where idcomputers = ".$id." ";
            require_once "model/dbConnector.php";
            executeQueryWithNoReturn($updateQuery); //update
            $isUpdateComputerInDB = true;
        }
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    return $isUpdateComputerInDB;
}
/**
 * This function is designed to retrieve the user's built saved computers from the data base
 * @param $username string
 * @return array returns all data from a built computer
 */
function getUserBuilds($username): array
{
    //set local variables
    $strSep = '\'';
    $results = [];
    $userInformation = [];
    try{
        require_once "model/userManagement.php";
        $userInformation = getUserInfo($username);

    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }

    $userID = $userInformation[0]["idusers"]; //set the user's ID
    try{
        $getBuildsQuery = 'SELECT * FROM computers where users_idusers='.$strSep.$userID.$strSep.' AND example = 0';
        require_once "model/dbConnector.php";
        $results = executeQuery($getBuildsQuery);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    return $results;
}
/**
 * This function is designed to retrieve the admin's saved example computers from the data base
 * @return array returns all data from a built computers
 */
function getExampleBuilds(): array
{
    $results = [];
    try{
        $getBuildsQuery = 'SELECT * FROM computers where example = 1';
        require_once "model/dbConnector.php";
        $results = executeQuery($getBuildsQuery);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    return $results;
}

/**
 * This function is designed to retrieve the user's built saved computers from the data base
 * @param $username string
 * @param $buildID int
 * @return array returns a built computer which contains it's components
 */
function getUserBuild($username,$buildID): array
{
    $strSep = '\'';
    $results = [];
    $userInformation = [];
    try{
        require_once "model/userManagement.php";
        $userInformation = getUserInfo($username);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    $userID = $userInformation[0]["idusers"];
    try{
        $getBuildsQuery = 'SELECT * FROM computers where (idcomputers ='.$buildID.' AND users_idusers ='.$userID.') OR (example = 1 and idcomputers ='.$buildID.') ';
        require_once "model/dbConnector.php";
        $results = executeQuery($getBuildsQuery);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    return $results;
}

/**
 * This function is designed to retrieve all components that was in a build
 * @param $build array
 * @return array returns a list of components that was contained in a build
 */
function getComponentsByBuild($build): array
{
    $results = [];
    require_once "model/inventoryManagement.php";
    //check if the component has been selected and get all information from the data base and push it into an array
    if($build[0]["cases_idcases"] != "")
    {
        array_push($results,getComponentInfo("cases",$build[0]["cases_idcases"]));
    }
    if($build[0]["motherboard_idmotherboard"] != "")
    {
        array_push($results,getComponentInfo("motherboard",$build[0]["motherboard_idmotherboard"]));
    }
    if($build[0]["processors_idprocessors"] != "")
    {
        array_push($results,getComponentInfo("processors",$build[0]["processors_idprocessors"]));
    }
    if($build[0]["storage_idstorage"] != "")
    {
        array_push($results,getComponentInfo("storage",$build[0]["storage_idstorage"]));
    }
    if($build[0]["power_supply_idpower_supply"] != "")
    {
        array_push($results,getComponentInfo("power_supply",$build[0]["power_supply_idpower_supply"]));
    }
    if($build[0]["graphics_cards_idgraphics_cards"] != "")
    {
        array_push($results,getComponentInfo("graphics_cards",$build[0]["graphics_cards_idgraphics_cards"]));
    }
    if($build[0]["ram_idram"] != "")
    {
        array_push($results,getComponentInfo("ram",$build[0]["ram_idram"]));
    }
    if($build[0]["coolers_idcoolers"] != "")
    {
        array_push($results,getComponentInfo("coolers",$build[0]["coolers_idcoolers"]));
    }

    return $results;
}

/**
 * This function is designed to get all components by a built computer
 * @param $username string
 * @param $buildID int
 * @return array returns an array of components of a build. -> Contains everything about the built computer.
 */
function getComponentsByBuildID($username,$buildID): array
{
    $buildResult = getUserBuild($username,$buildID);
    $results = getComponentsByBuild($buildResult);
    return $results; //return a list of components
}

/**
 * This funciton is designed to delete a user's selected computer.
 * @param $username string
 * @param $buildID int
 * @return bool returns true if the deleted build was successful
 */
function isDeleteBuildCorrect($username,$buildID): bool
{
    //Set local variables
    $isDeleteBuildCorrect = false;
    $userInformation = null;
    $strSep = '\'';

    try{
        require_once "model/userManagement.php";
        $userInformation = getUserInfo($username);

    }catch(Exception $exception)
    {
        echo "User doesn't exist";
    }
    $userID = $userInformation[0]['idusers'];
    $computerResult = [];
    try{
        $query = "SELECT idcomputers from computers where users_idusers =".$strSep.$userID.$strSep." AND idcomputers =".$strSep.$buildID.$strSep." ";
        require_once "model/dbConnector.php";
        $computerResult = executeQuery($query);
    }catch(Exception $exception)
    {
        echo "Selected build doesn't exist.";
    }
    try{
        //Check if the computer exists
        if(count($computerResult) == 1)
        {
            $deleteQuery = "DELETE FROM computers where idcomputers =".$strSep.$computerResult[0][0].$strSep;
            require_once "model/dbConnector.php";
            executeQueryWithNoReturn($deleteQuery);
            $isDeleteBuildCorrect = true;
        }
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }

    return $isDeleteBuildCorrect;
}

/**
 * This function is designed to delete an example computer
 * @param $buildID int
 * @return bool returns true when the computer has been deleted successfully
 */
function isDeleteExampleBuildCorrect($buildID): bool
{
    $isDeleteExampleBuildCorrect = false;
    $strSep = '\'';
    $computerResult = [];
    try{
        $query = "SELECT idcomputers from computers where idcomputers =".$buildID." ";
        require_once "model/dbConnector.php";
        $computerResult = executeQuery($query);
    }catch(Exception $exception)
    {
        echo "Selected build doesn't exist.";
    }
    try{
        if(count($computerResult) == 1)
        {
            $deleteQuery = "DELETE FROM computers where idcomputers =".$strSep.$computerResult[0][0].$strSep;
            require_once "model/dbConnector.php";
            executeQueryWithNoReturn($deleteQuery);
            $isDeleteExampleBuildCorrect = true;
        }
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }

    return $isDeleteExampleBuildCorrect;
}

/**
 * This funciton is designed to check if the build is a user build
 * @param $buildID int
 * @return bool returns true
 */
function isExampleBuildbyID($buildID): bool
{
    //set local variables
    $isExampleBuildbyID = false;
    $computerResult = [];
    try{
        $query = "SELECT example from computers where idcomputers =".$buildID." ";
        require_once "model/dbConnector.php";
        $computerResult = executeQuery($query);
    }catch(Exception $exception)
    {
        echo "Selected build doesn't exist.";
    }
    if((int)$computerResult[0]["example"] == 1)
    {
        $isExampleBuildbyID = true;
    }
    return $isExampleBuildbyID;
}