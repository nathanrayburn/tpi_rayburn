-- --------------------------------------------------------
-- Host:                         web22.swisscenter.com
-- Server version:               5.7.34-37-log - Percona Server (GPL), Release 37, Revision 7c516e9
-- Server OS:                    Linux
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for tpi_nrn_admin
DROP DATABASE IF EXISTS `tpi_nrn_admin`;
CREATE DATABASE IF NOT EXISTS `tpi_nrn_admin` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tpi_nrn_admin`;

-- Dumping structure for table tpi_nrn_admin.cases
DROP TABLE IF EXISTS `cases`;
CREATE TABLE IF NOT EXISTS `cases` (
  `idcases` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `form_factor_motherboard` varchar(255) NOT NULL,
  `max_length_gpu` int(11) NOT NULL,
  `max_height_cpu` int(11) NOT NULL,
  `form_factor_power_supply` varchar(255) NOT NULL,
  `aio_water` tinyint(4) DEFAULT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idcases`),
  UNIQUE KEY `idcases_UNIQUE` (`idcases`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.computers
DROP TABLE IF EXISTS `computers`;
CREATE TABLE IF NOT EXISTS `computers` (
  `idcomputers` int(11) NOT NULL AUTO_INCREMENT,
  `example` tinyint(4) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `registration_date` datetime DEFAULT NULL,
  `users_idusers` int(11) NOT NULL,
  `cases_idcases` int(11) DEFAULT NULL,
  `motherboard_idmotherboard` int(11) DEFAULT NULL,
  `processors_idprocessors` int(11) DEFAULT NULL,
  `storage_idstorage` int(11) DEFAULT NULL,
  `power_supply_idpower_supply` int(11) DEFAULT NULL,
  `ram_idram` int(11) DEFAULT NULL,
  `graphics_cards_idgraphics_cards` int(11) DEFAULT NULL,
  `coolers_idcoolers` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcomputers`),
  KEY `fk_computers_users_idx` (`users_idusers`),
  KEY `fk_computers_cases1_idx` (`cases_idcases`),
  KEY `fk_computers_motherboard1_idx` (`motherboard_idmotherboard`),
  KEY `fk_computers_processors1_idx` (`processors_idprocessors`),
  KEY `fk_computers_storage1_idx` (`storage_idstorage`),
  KEY `fk_computers_power_supply1_idx` (`power_supply_idpower_supply`),
  KEY `fk_computers_ram1_idx` (`ram_idram`),
  KEY `fk_computers_graphics_cards1_idx` (`graphics_cards_idgraphics_cards`),
  KEY `fk_computers_coolers1_idx` (`coolers_idcoolers`),
  CONSTRAINT `fk_computers_cases1` FOREIGN KEY (`cases_idcases`) REFERENCES `cases` (`idcases`),
  CONSTRAINT `fk_computers_coolers1` FOREIGN KEY (`coolers_idcoolers`) REFERENCES `coolers` (`idcoolers`),
  CONSTRAINT `fk_computers_graphics_cards1` FOREIGN KEY (`graphics_cards_idgraphics_cards`) REFERENCES `graphics_cards` (`idgraphics_cards`),
  CONSTRAINT `fk_computers_motherboard1` FOREIGN KEY (`motherboard_idmotherboard`) REFERENCES `motherboard` (`idmotherboard`),
  CONSTRAINT `fk_computers_power_supply1` FOREIGN KEY (`power_supply_idpower_supply`) REFERENCES `power_supply` (`idpower_supply`),
  CONSTRAINT `fk_computers_processors1` FOREIGN KEY (`processors_idprocessors`) REFERENCES `processors` (`idprocessors`),
  CONSTRAINT `fk_computers_ram1` FOREIGN KEY (`ram_idram`) REFERENCES `ram` (`idram`),
  CONSTRAINT `fk_computers_storage1` FOREIGN KEY (`storage_idstorage`) REFERENCES `storage` (`idstorage`),
  CONSTRAINT `fk_computers_users` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.coolers
DROP TABLE IF EXISTS `coolers`;
CREATE TABLE IF NOT EXISTS `coolers` (
  `idcoolers` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `socket_support` varchar(255) NOT NULL,
  `max_height` int(11) NOT NULL,
  `aio_water` tinyint(4) DEFAULT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idcoolers`),
  UNIQUE KEY `idcoolers_UNIQUE` (`idcoolers`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.graphics_cards
DROP TABLE IF EXISTS `graphics_cards`;
CREATE TABLE IF NOT EXISTS `graphics_cards` (
  `idgraphics_cards` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `max_length_gpu` int(11) NOT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idgraphics_cards`),
  UNIQUE KEY `idgraphics_cards_UNIQUE` (`idgraphics_cards`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.motherboard
DROP TABLE IF EXISTS `motherboard`;
CREATE TABLE IF NOT EXISTS `motherboard` (
  `idmotherboard` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `socket_cpu` varchar(255) NOT NULL,
  `form_factor` varchar(255) NOT NULL,
  `form_factor_ram` varchar(255) NOT NULL,
  `interface_m2` varchar(255) DEFAULT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idmotherboard`),
  UNIQUE KEY `idmotherboard_UNIQUE` (`idmotherboard`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.power_supply
DROP TABLE IF EXISTS `power_supply`;
CREATE TABLE IF NOT EXISTS `power_supply` (
  `idpower_supply` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `form_factor` varchar(255) NOT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idpower_supply`),
  UNIQUE KEY `idpower_supply_UNIQUE` (`idpower_supply`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.processors
DROP TABLE IF EXISTS `processors`;
CREATE TABLE IF NOT EXISTS `processors` (
  `idprocessors` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `socket_cpu` varchar(255) NOT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idprocessors`),
  UNIQUE KEY `idprocessors_UNIQUE` (`idprocessors`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.ram
DROP TABLE IF EXISTS `ram`;
CREATE TABLE IF NOT EXISTS `ram` (
  `idram` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `form_factor` varchar(255) NOT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idram`),
  UNIQUE KEY `idram_UNIQUE` (`idram`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.storage
DROP TABLE IF EXISTS `storage`;
CREATE TABLE IF NOT EXISTS `storage` (
  `idstorage` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `form_factor` varchar(255) NOT NULL,
  `interface_m2` varchar(255) DEFAULT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idstorage`),
  UNIQUE KEY `idstorage_UNIQUE` (`idstorage`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table tpi_nrn_admin.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `idusers` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`idusers`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
