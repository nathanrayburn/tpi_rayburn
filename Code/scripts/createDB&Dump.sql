-- --------------------------------------------------------
-- Host:                         web22.swisscenter.com
-- Server version:               5.7.34-37-log - Percona Server (GPL), Release 37, Revision 7c516e9
-- Server OS:                    Linux
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for tpi_nrn_admin
CREATE DATABASE IF NOT EXISTS `tpi_nrn_admin` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tpi_nrn_admin`;

-- Dumping structure for table tpi_nrn_admin.cases
CREATE TABLE IF NOT EXISTS `cases` (
  `idcases` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `form_factor_motherboard` varchar(255) NOT NULL,
  `max_length_gpu` int(11) NOT NULL,
  `max_height_cpu` int(11) NOT NULL,
  `form_factor_power_supply` varchar(255) NOT NULL,
  `aio_water` tinyint(4) DEFAULT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idcases`),
  UNIQUE KEY `idcases_UNIQUE` (`idcases`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.cases: ~3 rows (approximately)
/*!40000 ALTER TABLE `cases` DISABLE KEYS */;
INSERT INTO `cases` (`idcases`, `name`, `description`, `image`, `price`, `form_factor_motherboard`, `max_length_gpu`, `max_height_cpu`, `form_factor_power_supply`, `aio_water`, `public_visibility`) VALUES
	(70, 'intertech cxc2 blue', 'this is a great case', 'img/admin-cases-2021-6-03-04-55-27.png', 150, 'atx', 300, 165, 'atx', 0, 1),
	(75, 'dark purple case', 'this case is amazing with it\'s purple details !', 'img/admin-cases-2021-6-04-09-35-29.png', 190, 'atx', 200, 150, 'atx', 0, 1),
	(76, 'shark purple case', 'this is a great and spacious case for atx motherboards and big graphics cards !', 'img/admin-cases-2021-6-04-11-08-40.png', 165, 'atx', 200, 160, 'atx', 1, 1);
/*!40000 ALTER TABLE `cases` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.computers
CREATE TABLE IF NOT EXISTS `computers` (
  `idcomputers` int(11) NOT NULL AUTO_INCREMENT,
  `example` tinyint(4) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `registration_date` datetime DEFAULT NULL,
  `users_idusers` int(11) NOT NULL,
  `cases_idcases` int(11) DEFAULT NULL,
  `motherboard_idmotherboard` int(11) DEFAULT NULL,
  `processors_idprocessors` int(11) DEFAULT NULL,
  `storage_idstorage` int(11) DEFAULT NULL,
  `power_supply_idpower_supply` int(11) DEFAULT NULL,
  `ram_idram` int(11) DEFAULT NULL,
  `graphics_cards_idgraphics_cards` int(11) DEFAULT NULL,
  `coolers_idcoolers` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcomputers`),
  KEY `fk_computers_users_idx` (`users_idusers`),
  KEY `fk_computers_cases1_idx` (`cases_idcases`),
  KEY `fk_computers_motherboard1_idx` (`motherboard_idmotherboard`),
  KEY `fk_computers_processors1_idx` (`processors_idprocessors`),
  KEY `fk_computers_storage1_idx` (`storage_idstorage`),
  KEY `fk_computers_power_supply1_idx` (`power_supply_idpower_supply`),
  KEY `fk_computers_ram1_idx` (`ram_idram`),
  KEY `fk_computers_graphics_cards1_idx` (`graphics_cards_idgraphics_cards`),
  KEY `fk_computers_coolers1_idx` (`coolers_idcoolers`),
  CONSTRAINT `fk_computers_cases1` FOREIGN KEY (`cases_idcases`) REFERENCES `cases` (`idcases`),
  CONSTRAINT `fk_computers_coolers1` FOREIGN KEY (`coolers_idcoolers`) REFERENCES `coolers` (`idcoolers`),
  CONSTRAINT `fk_computers_graphics_cards1` FOREIGN KEY (`graphics_cards_idgraphics_cards`) REFERENCES `graphics_cards` (`idgraphics_cards`),
  CONSTRAINT `fk_computers_motherboard1` FOREIGN KEY (`motherboard_idmotherboard`) REFERENCES `motherboard` (`idmotherboard`),
  CONSTRAINT `fk_computers_power_supply1` FOREIGN KEY (`power_supply_idpower_supply`) REFERENCES `power_supply` (`idpower_supply`),
  CONSTRAINT `fk_computers_processors1` FOREIGN KEY (`processors_idprocessors`) REFERENCES `processors` (`idprocessors`),
  CONSTRAINT `fk_computers_ram1` FOREIGN KEY (`ram_idram`) REFERENCES `ram` (`idram`),
  CONSTRAINT `fk_computers_storage1` FOREIGN KEY (`storage_idstorage`) REFERENCES `storage` (`idstorage`),
  CONSTRAINT `fk_computers_users` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.computers: ~6 rows (approximately)
/*!40000 ALTER TABLE `computers` DISABLE KEYS */;
INSERT INTO `computers` (`idcomputers`, `example`, `image`, `registration_date`, `users_idusers`, `cases_idcases`, `motherboard_idmotherboard`, `processors_idprocessors`, `storage_idstorage`, `power_supply_idpower_supply`, `ram_idram`, `graphics_cards_idgraphics_cards`, `coolers_idcoolers`) VALUES
	(91, 1, 'img/admin-computers-2021-6-03-04-11-05.png', '2021-06-03 10:03:08', 2, 70, 1, 1, 1, 1, 1, 1, 1),
	(105, 1, 'img/admin-computers-2021-6-04-11-18-56.png', '2021-06-03 09:03:10', 2, 76, 2, 2, 1, 1, 2, 2, 3),
	(106, 0, '', '2021-06-03 09:03:12', 2, 70, NULL, 1, NULL, NULL, NULL, NULL, 1),
	(111, 0, '', '2021-06-03 08:03:08', 23, 70, 1, 1, NULL, 1, 1, 1, 1),
	(118, 1, 'img/admin-computers-2021-6-04-11-37-48.png', '2021-06-04 09:36:20', 2, 75, 1, 1, 3, 3, 1, 2, 1),
	(122, 0, '', '2021-06-04 13:03:35', 19, 75, 1, 1, NULL, NULL, 1, 2, 1);
/*!40000 ALTER TABLE `computers` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.coolers
CREATE TABLE IF NOT EXISTS `coolers` (
  `idcoolers` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `socket_support` varchar(255) NOT NULL,
  `max_height` int(11) NOT NULL,
  `aio_water` tinyint(4) DEFAULT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idcoolers`),
  UNIQUE KEY `idcoolers_UNIQUE` (`idcoolers`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.coolers: ~3 rows (approximately)
/*!40000 ALTER TABLE `coolers` DISABLE KEYS */;
INSERT INTO `coolers` (`idcoolers`, `name`, `description`, `image`, `price`, `socket_support`, `max_height`, `aio_water`, `public_visibility`) VALUES
	(1, 'dark rock pro 4', 'the two silent wings pwm fans are equipped with durable fluid dynamic bearings, smooth-running 6-pole motors and airflow-optimized fan blades. the front fan is a silent wings 3, which ...', 'img/admin-coolers-2021-6-03-04-55-49.png', 81.9, 'lga1200', 162, 0, 1),
	(2, 'dark rock pro 3', 'shadow rock 3 offers impressive cooling and whisper quiet operation. perfect for everyone. the shadow rock 3 offers impressive cooling performance. it\'s the perfect choice for design-oriented users looking for quiet operation and high performance for their air-cooled system. the two-tone brushed aluminium top cover also impresses with its exceptional elegance in this premium cooler segment.', '', 109, 'lga1151', 160, 0, 1),
	(3, 'kraken x53 aio cpu water cooler - 240mm', 'the refreshed kraken x series is a must-have for any build, providing better cooling, stunning visual effects, and intuitive installation. you can fine-tune settings with cam’s software interface, ensuring optimal performance in even the most intense gaming sessions. the new kraken x series maintains the customizable lighting effects and quiet performance of previous generations and adds a hue 2 connector for even more rgb options. with a re-designed cap and larger infinity mirror ring led, the new kraken x delivers an amazing experience in rgb liquid cooling, backed by a 6-year warranty.', 'img/admin-coolers-2021-6-04-11-10-28.jpg', 120, 'am4', 140, 1, 1);
/*!40000 ALTER TABLE `coolers` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.graphics_cards
CREATE TABLE IF NOT EXISTS `graphics_cards` (
  `idgraphics_cards` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `max_length_gpu` int(11) NOT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idgraphics_cards`),
  UNIQUE KEY `idgraphics_cards_UNIQUE` (`idgraphics_cards`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.graphics_cards: ~2 rows (approximately)
/*!40000 ALTER TABLE `graphics_cards` DISABLE KEYS */;
INSERT INTO `graphics_cards` (`idgraphics_cards`, `name`, `description`, `image`, `price`, `max_length_gpu`, `public_visibility`) VALUES
	(1, 'jeu asus rog strix rtx 3090 o24g', 'conception reconditionnée avec d\'excellentes performances thermiques', '', 2999.9, 250, 1),
	(2, 'rtx 3070 phoenix', 'this is the best graphics card in the market !', 'img/admin-graphics_cards-2021-6-03-04-56-44.jpg', 890, 180, 1);
/*!40000 ALTER TABLE `graphics_cards` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.motherboard
CREATE TABLE IF NOT EXISTS `motherboard` (
  `idmotherboard` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `socket_cpu` varchar(255) NOT NULL,
  `form_factor` varchar(255) NOT NULL,
  `form_factor_ram` varchar(255) NOT NULL,
  `interface_m2` varchar(255) DEFAULT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idmotherboard`),
  UNIQUE KEY `idmotherboard_UNIQUE` (`idmotherboard`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.motherboard: ~2 rows (approximately)
/*!40000 ALTER TABLE `motherboard` DISABLE KEYS */;
INSERT INTO `motherboard` (`idmotherboard`, `name`, `description`, `image`, `price`, `socket_cpu`, `form_factor`, `form_factor_ram`, `interface_m2`, `public_visibility`) VALUES
	(1, 'asus tuf gaming x570-plus', 'cool', 'img/admin-motherboard-2021-6-04-09-38-23.jpg', 199, 'lga1200', 'atx', 'dimm288', '2280', 1),
	(2, 'amd asus motherboard 2719', 'this motherboard is made for the latest ryzen cpu !', 'img/admin-motherboard-2021-6-04-11-19-56.jpg', 152, 'am4', 'atx', 'dimm288', 'nvme', 1);
/*!40000 ALTER TABLE `motherboard` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.power_supply
CREATE TABLE IF NOT EXISTS `power_supply` (
  `idpower_supply` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `form_factor` varchar(255) NOT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idpower_supply`),
  UNIQUE KEY `idpower_supply_UNIQUE` (`idpower_supply`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.power_supply: ~2 rows (approximately)
/*!40000 ALTER TABLE `power_supply` DISABLE KEYS */;
INSERT INTO `power_supply` (`idpower_supply`, `name`, `description`, `image`, `price`, `form_factor`, `public_visibility`) VALUES
	(1, 'corsair 800w power supply', 'great power supply for gaming', 'img/admin-power_supply-2021-6-04-11-22-46.jpg', 124, 'atx', 1),
	(3, 'be quiet 700 watt', 'this power supply is the quietest gold rated in the world', 'img/admin-power_supply-2021-6-04-11-37-04.jpg', 89, 'atx', 1);
/*!40000 ALTER TABLE `power_supply` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.processors
CREATE TABLE IF NOT EXISTS `processors` (
  `idprocessors` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `socket_cpu` varchar(255) NOT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idprocessors`),
  UNIQUE KEY `idprocessors_UNIQUE` (`idprocessors`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.processors: ~2 rows (approximately)
/*!40000 ALTER TABLE `processors` DISABLE KEYS */;
INSERT INTO `processors` (`idprocessors`, `name`, `description`, `image`, `price`, `socket_cpu`, `public_visibility`) VALUES
	(1, 'intel i910850k', '10-core/20 threads\r\nup to 5.2 ghz unlocked\r\nintel uhd graphics 630\r\ncompatible with intel 400 series chipset based motherboards\r\ncompatible with intel 500 series chipset based motherboards\r\nintel turbo boost max technology 3.0 support', 'img/admin-processors-2021-6-03-04-55-38.jpg', 450.23, 'lga1200', 1),
	(2, 'ryzen 9 5000', 'this is the latest and greatest processor. it has an amazing 32 cores ! ', 'img/admin-processors-2021-6-04-09-43-28.png', 650, 'am4', 1);
/*!40000 ALTER TABLE `processors` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.ram
CREATE TABLE IF NOT EXISTS `ram` (
  `idram` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `form_factor` varchar(255) NOT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idram`),
  UNIQUE KEY `idram_UNIQUE` (`idram`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.ram: ~2 rows (approximately)
/*!40000 ALTER TABLE `ram` DISABLE KEYS */;
INSERT INTO `ram` (`idram`, `name`, `description`, `image`, `price`, `form_factor`, `public_visibility`) VALUES
	(1, 'ddr4, cl16, dimm, kit de 2, hyperx fury rgb', 'kingston 32 go, 3200 mhz, ddr4, cl16, dimm, kit de 2, hyperx fury rgb.', 'img/admin-ram-2021-6-04-09-39-07.jpg', 189, 'dimm288', 1),
	(2, 'kingston hyperx fury ddr4 32gb rgb memory', 'this ram is incredible with the fast speeds it delivers and the nice rgb details it adds to your computer !', 'img/admin-ram-2021-6-04-09-40-26.jpg', 190, 'dimm288', 1);
/*!40000 ALTER TABLE `ram` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.storage
CREATE TABLE IF NOT EXISTS `storage` (
  `idstorage` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` longtext,
  `price` float NOT NULL,
  `form_factor` varchar(255) NOT NULL,
  `interface_m2` varchar(255) DEFAULT NULL,
  `public_visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`idstorage`),
  UNIQUE KEY `idstorage_UNIQUE` (`idstorage`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.storage: ~3 rows (approximately)
/*!40000 ALTER TABLE `storage` DISABLE KEYS */;
INSERT INTO `storage` (`idstorage`, `name`, `description`, `image`, `price`, `form_factor`, `interface_m2`, `public_visibility`) VALUES
	(1, 'seagate barracuda 2tb', 'seagate harddisk barracuda 2tb 3.5, storage application area: desktop pc, total storage capacity: 2 tb, storage interface: sata iii (6gb/s), hard disk form factor: 3.5.', 'img/admin-storage-2021-6-03-04-57-59.jpg', 55.1, 'sata', '', 1),
	(2, 'kingston a2000 1000 go', 'le ssd a2000 nvme pcie de kingston est une solution de stockage abordable avec des performances impressionnantes à des vitesses de lecture / écriture allant jusqu\'à 2200/2000 mo / s. l\'a2000 offre des performances 3 fois supérieures à celles d\'un ssd sata - avec des temps de chargement plus rapides, moins de consommation d\'énergie et moins de génération de chaleur. le ssd a2000 a été spécialement développé pour les débutants, les fabricants de systèmes spécialisés, les fabricants de systèmes de bricolage et ceux qui souhaitent mettre à niveau leur pc. avec un design mince m.2 unilatéral, l\'a2000 déploie tout son potentiel lorsqu\'il est installé dans un ultrabook ou un système pc sff (pc à petit facteur de forme).', 'img/admin-storage-2021-6-03-04-59-02.jpg', 105, 'm.22280', 'nvme', 1),
	(3, 'seagate barracuda 500 gb', 'this hard drive is great for big files and long terme storage.', 'img/admin-storage-2021-6-04-09-46-37.jpg', 25, 'sata', '', 1);
/*!40000 ALTER TABLE `storage` ENABLE KEYS */;

-- Dumping structure for table tpi_nrn_admin.users
CREATE TABLE IF NOT EXISTS `users` (
  `idusers` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`idusers`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Dumping data for table tpi_nrn_admin.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`idusers`, `email`, `username`, `name`, `lastname`, `password`, `type`) VALUES
	(2, 'administrator@cpnv.ch', 'admin', 'Admin', 'administrateur', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'admin'),
	(13, 'raphael.favre@cpnv.com', 'Raphael', 'Raphael_Mod', 'Favre_mod', 'e9cee71ab932fde863338d08be4de9dfe39ea049bdafb342ce659ec5450b69ae', 'user'),
	(16, 'clauduss@swiss.com', 'clauduss', 'Claude', 'Duss', 'b49270563b97e1ccba5afc0bd60229117ad35f7016f89dcb68d195aadaea0815', 'user'),
	(19, 'nathanarmand@hotmail.com', 'nathanrayburn', 'Nathan', 'Rayburn', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'user'),
	(23, 'user.test@cpnv.ch', 'usertester', 'User', 'Test', 'ce2edcc04b886bfffcb5112dedda26744694824e4b42e8c8b3b69557fbedcd06', 'user');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
