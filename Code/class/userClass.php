<?php
/**
 * Title : User class
 * Author : Nathan Rayburn
 * Date : 17/05/21
 * Description : This file is designed to contain the user class
 */

/**
 * Class User
 * This class is designed to create objects for the current user
 */
class User{
    //properties
    public $email; //string
    public $username; //string
    public $firstname; //string
    public $lastname; //string

    //methods
    function set_email($email)
    {
        $this->email = $email;
    }
    function get_email()
    {
        return $this->email;
    }
    function set_username($username)
    {
        $this->username = $username;
    }
    function get_username()
    {
        return $this->username;
    }
    function set_firstname($firstname)
    {
        $this->firstname = $firstname;
    }
    function get_firstname()
    {
       return $this->firstname;
    }
    function set_lastname($lastname)
    {
        $this->lastname = $lastname;
    }
    function get_lastname()
    {
       return $this->lastname;
    }
}
?>