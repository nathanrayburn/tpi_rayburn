<?php
/**
 * Author : Nathan Rayburn
 * Date : 3/18/21
 * Description : This file is designed to navigate data between the model and view
 */
require 'vendor/autoload.php'; //load library to project

/**
 * This function is designed to display the login page and logs the user
 * @param $loginRequest array of inputs from HTML fields
 */
function login($loginRequest)
{
    if (isset($_SESSION["username"])) {
        $_Get["action"] = "menu";
        home();

    } else {
        //check if fields were filled
        if (isset($loginRequest["inputUserUsername"]) && isset($loginRequest["inputUserPassword"])) {
            //set variables from array
            $username = $loginRequest["inputUserUsername"];
            $userPassword = hash("sha256", $loginRequest["inputUserPassword"]); //hash password

            require "model/userManagement.php";
            //create session if login was successful
            if (isLoginCorrect($username, $userPassword)) {

                unset($_SESSION["loginError"]);

                $type = getUserType($username);

                createSession($username, $type);

                $_GET["action"] = "home";
                home(); //display
            } else {
                $_SESSION["loginError"] = 1;
                $_GET["action"] = "login";
                require "view/login.php"; //display login
            }
        } else {
            $_GET["action"] = "login";
            require "view/login.php"; //display login
        }
    }
}

/**
 * This function is designed to display the register page and register a new user
 * @param $arrayofinputs array of inputs from HTML field
 */
function register($arrayofinputs)
{
    //check if user has a session
    if (isset($_SESSION["username"])) {
        $_Get["action"] = "home";
        home(); //display home

    } else {
        //check if all fields were filled
        if (isset($arrayofinputs["registerUserEmailAddress"]) && isset($arrayofinputs["registerUserPassword"]) && isset($arrayofinputs["registerRepeatUserPassword"]) && isset($arrayofinputs["registerUserFirstname"]) && isset($arrayofinputs["registerUserLastname"]) && isset($arrayofinputs["registerUserUsername"])) {
            //set variables from array
            $userEmail = $arrayofinputs["registerUserEmailAddress"];
            $userPassword = $arrayofinputs["registerUserPassword"];
            $userRepeatPassword = $arrayofinputs["registerRepeatUserPassword"];
            $userFirstname = $arrayofinputs["registerUserFirstname"];
            $userLastname = $arrayofinputs["registerUserLastname"];
            $username = $arrayofinputs["registerUserUsername"];

            unset($_SESSION["registerPasswordError"]);
            unset($_SESSION["registerError"]);

            //check if the passwords are the same
            if ($userPassword == $userRepeatPassword) {
                require "model/userManagement.php";
                ///when registration is correct, creates a new session
                if (isRegisterCorrect($username, $userPassword, $userEmail, $userFirstname, $userLastname)) {

                    $type = getUserType($username);
                    createSession($username, $type); //create session

                    $_GET["action"] = "home";
                    home(); //display home
                } else {
                    $_SESSION["registerError"] = 1;
                    $_GET["action"] = "register";
                    require "view/register.php"; //display register
                }
            } else {
                $_SESSION["registerPasswordError"] = 1;
                $_GET["action"] = "register";
                require "view/register.php"; //display register
            }
        } else {
            unset($_SESSION["registerError"]);
            $_GET["action"] = "register";
            require "view/register.php"; //display register
        }
    }
}

/**
 * This function is designed to get the user's type from DB
 * @param $username string
 * @return array|null returns the user's type
 */
function getUserType($username): ?array
{
    return getUserTypeFromDB($username); //return array of string
}

/**
 * This function is designed to create a new session with the user's e-mail and username
 * @param $username string
 * @param $type array
 */
function createSession($username, $type)
{
    $_SESSION["username"] = $username;
    $_SESSION["userType"] = $type[0]["type"];
}

/**
 * This function is designed to logout a user, destroys the session and sends them back to home
 */
function logout()
{
    $_SESSION = array();
    session_destroy();
    $_GET["action"] = "home";
    require "view/login.php";
}

/**
 * This function is designed to display the home
 */
function home()
{
    require_once "model/computerManagement.php";
    $exampleComputers = getExampleBuilds(); //get example builds from data base
    $_GET["action"] = "home";
    require "view/home.php";
}

/**
 * This function is designed to display the admin panel
 */
function admin()
{
    if(isset($_SESSION["username"]))
    {
        if ($_SESSION["userType"] == "admin") {
            require "view/admin.php";
        } else {
            home();
        }
    }else{
        require "view/login.php";
    }
}

/**
 * This function is designed to display the user manager page
 */
function userManager()
{
    //check if user connected
    if(isset($_SESSION["username"]))
    {
        //check if the user is admin
        if ($_SESSION["userType"] == "admin") {

            $userType = "user";

            require_once "model/userManagement.php";

            $results = getAllUsersInfo($userType);
            $users = createClassesForAllUsers($results);

            require "view/userManager.php";
        } else {
            home();
        }
    }else{
        require "view/login.php"; //display login
    }
}

/**
 * This function is designed to display the admin's model manager
 */
function modelManager()
{
    if ($_SESSION["userType"] == "admin") {
        require_once "model/computerManagement.php";
        $results = getExampleBuilds();
        require "view/modelManager.php";
    } else {
        home();
    }
}

/***
 * This function is designed to display the admin's inventory manager
 */
function inventoryManager()
{
    if ($_SESSION["userType"] == "admin") {
        require "view/inventoryManager.php";
    } else {
        home();
    }
}

/**
 * This function is designed to display the list of components for the admin
 * @param $category string
 */
function componentList($category)
{
    if ($_SESSION["userType"] == "admin") {

        $_GET["action"] = "componentList";
        require_once "model/inventoryManagement.php";

        $results = inventoryComponentListAdmin($category);
        $title = formatTitle($category);

        require "view/inventoryComponentList.php";
    } else {
        home();
    }
}

/**
 * This function is designed to display the user's profile
 */
function profile()
{
    if (isset($_SESSION["username"])) {
        require "model/userManagement.php";
        $result = getUserInfo($_SESSION["username"]);
        //creates the user's class has been created
        if (createClass($result, "current")) {
            require "view/userProfileDefault.php";
        }
    } else {
        require "view/login.php";
    }
}

/**
 * This funciton is designed to display the user's profile editor
 * @param $arrayofinputs array of string from HTML inputs
 */
function profileInformation($arrayofinputs)
{
    if (isset($_SESSION["username"])) {
        if (isset($_SESSION["userInformation"])) {
            //check if the inputs were filled
            if (isset($arrayofinputs["registerUserEmailAddress"]) && isset($arrayofinputs["registerUserFirstname"]) && isset($arrayofinputs["registerUserLastname"])) {
                //set variables from array
                $email = $arrayofinputs["registerUserEmailAddress"];
                $firstname = $arrayofinputs["registerUserFirstname"];
                $lastname = $arrayofinputs["registerUserLastname"];
                $username = $_SESSION["username"];

                require "model/userManagement.php";
                //update profile
                if (isProfileUpdateCorrect($email, $firstname, $lastname, $username)) {
                    $result = getUserInfo($_SESSION["username"]);
                    if (createClass($result, "current")) { //create class
                        require "view/userProfileDefault.php"; //return to profile
                    }
                } else {
                    $_SESSION["updateError"] = 1;
                    require "view/userProfileInformation.php";
                }
            } else {
                $_GET["action"] = "profileInformation";
                require "view/userProfileInformation.php";
            }
        } else {
            $_GET["action"] = "profile";
            require "view/userProfileDefault.php";
        }
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the form to change a user password
 * @param $arrayofinputs array of inputs from HTML form
 */
function userPassword($arrayofinputs)
{
    if (isset($_SESSION["username"])) {
        if (isset($arrayofinputs["repeatedNewPassword"]) && isset($arrayofinputs["newPassword"]) && isset($arrayofinputs["currentPassword"])) {
            $currentPassword = $arrayofinputs["currentPassword"];
            $newPassword = $arrayofinputs["newPassword"];
            $repeatedNewPassword = $arrayofinputs["repeatedNewPassword"];
            $username = $_SESSION["username"];
            unset($_SESSION["passwordError"]);
            require "model/userManagement.php";
            if (isCurrentPasswordCorrect($currentPassword, $username) && isNewPasswordSame($newPassword, $repeatedNewPassword)) {
                //update password into data base
                if (isUpdateUserPassword($username, $newPassword)) {
                    $_SESSION["passwordError"] = 0;
                } else {
                    $_SESSION["passwordError"] = 1;
                }
                //get the user's information
                $result = getUserInfo($_SESSION["username"]);
                if (createClass($result, "current")) {
                    require "view/userProfileDefault.php"; //return to profile
                }
            } else {
                $_SESSION["passwordError"] = 1;
                require "view/userPasswordChange.php";
            }
        } else {
            require "view/userPasswordChange.php";
        }
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the page to create a new user
 *
 * @param $arrayofinputs array(string)
 */
function createNewUser($arrayofinputs)
{
    if (isset($_SESSION["username"])) {
        if (isset($_SESSION["userType"]) == "admin") {
            unset($_SESSION["createError"]);
            //check if all inputs were filled
            if (isset($arrayofinputs["registerUserFirstname"]) && isset($arrayofinputs["registerUserLastname"]) && isset($arrayofinputs["registerUserEmailAddress"]) && isset($arrayofinputs["registerUserUsername"]) && isset($arrayofinputs["registerUserPassword"])) {

                //set variables from array
                $username = $arrayofinputs["registerUserUsername"];
                $firstname = $arrayofinputs["registerUserFirstname"];
                $lastname = $arrayofinputs["registerUserLastname"];
                $userPassword = $arrayofinputs["registerUserPassword"];
                $email = $arrayofinputs["registerUserEmailAddress"];

                require "model/userManagement.php";
                //check if the user's registers correctly
                if (isRegisterCorrect($username, $userPassword, $email, $firstname, $lastname)) {
                    $_SESSION["createError"] = 0;
                    userManager();
                } else {
                    $_SESSION["createError"] = 1;
                    require "view/userCreate.php";
                }
            } else {

                require "view/userCreate.php";
            }
        } else {
            home();
        }
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the page to edit a user
 * @param $arrayofinputs
 * @param $username
 */
function editUser($arrayofinputs, $username)
{
    if (isset($_SESSION["username"])) {
        if (isset($_SESSION["userType"]) == "admin") {
            if (isset($arrayofinputs["registerUserFirstname"]) && isset($arrayofinputs["registerUserLastname"]) && isset($arrayofinputs["registerUserEmailAddress"]) && isset($arrayofinputs["newPassword"])) {

                $firstname = $arrayofinputs["registerUserFirstname"];
                $lastname = $arrayofinputs["registerUserLastname"];
                $newPassword = $arrayofinputs["newPassword"];
                $email = $arrayofinputs["registerUserEmailAddress"];

                unset($_SESSION["updateError"]);
                require "model/userManagement.php";
                //check if update user information from admin was successful
                if (isUpdateUserInformationAdmin($firstname, $lastname, $email, $newPassword, $username)) {
                    $_SESSION["updateError"] = 0;
                    userManager();
                } else {
                    $_SESSION["updateError"] = 1;
                    require "view/userEdit.php";
                }

            } else {
                //check if username isn't null or empty
                if ($username != "" || $username != null) {
                    unset($_SESSION["updateError"]);

                    require "model/userManagement.php";
                    $result = getUserInfo($username);

                    createClass($result, "selected");

                    require "view/userEdit.php";
                } else {
                    userManager();
                }
            }
        } else {
            home();
        }
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed display the page to create a build
 * @param $type string
 * @param $computerID int
 */
function createBuild($type,$computerID)
{
    if (isset($_SESSION["username"])) {
        $type = (int)$type; //turn to int, 0 = user build 1 = example build

        unset($_SESSION["createBuildType"]);
        unset($_SESSION["selected.cases"]);
        unset($_SESSION["selected.coolers"]);
        unset($_SESSION["selected.processors"]);
        unset($_SESSION["selected.ram"]);
        unset($_SESSION["selected.graphics_cards"]);
        unset($_SESSION["selected.storage"]);
        unset($_SESSION["selected.power_supply"]);
        unset($_SESSION["selected.motherboard"]);
        unset($_SESSION["selected.buildID"]);

        //check if id isn't null
        if (!null == $computerID) {
            require_once "model/computerManagement.php";
            //verify if the computer is an example build
            if((!isExampleBuildbyID($computerID) && $type == 0) || (isExampleBuildbyID($computerID) && $_SESSION["userType"] == "admin"))
            {
                $_SESSION["selected.buildID"] = $computerID;
            }
            $results = getComponentsByBuildID($_SESSION["username"], $computerID);
            foreach ($results as $component) { //set selected component values
                foreach ($component[0] as $key => $value) {
                    if ($key == "idcases") {
                        $_SESSION["selected.cases"] = $component;
                        break;
                    }
                    if ($key == "idcoolers") {
                        $_SESSION["selected.coolers"] = $component;
                        break;
                    }
                    if ($key == "idprocessors") {
                        $_SESSION["selected.processors"] = $component;
                        break;
                    }
                    if ($key == "idram") {
                        $_SESSION["selected.ram"] = $component;
                        break;
                    }
                    if ($key == "idgraphics_cards") {
                        $_SESSION["selected.graphics_cards"] = $component;
                        break;
                    }
                    if ($key == "idmotherboard") {
                        $_SESSION["selected.motherboard"] = $component;
                        break;
                    }
                    if ($key == "idpower_supply") {
                        $_SESSION["selected.power_supply"] = $component;
                        break;
                    }
                    if ($key == "idstorage") {
                        $_SESSION["selected.storage"] = $component;
                        break;
                    }
                }
            }
        }
        if($type === 1 || $type === 0)
        {
            if($type === 1)
            {
                $_SESSION["createBuildType"] = 1;
                if($_SESSION["userType"] == "admin")
                {
                    require "view/modelBuilderSystem.php";
                }else{
                    home();
                }

            }elseif($type === 0)
            {
                require "view/userBuilderSystem.php";
            }
        }else{
            home();
        }

    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the page to the menu of the PC builder
 */
function userBuildMenu()
{
    if (isset($_SESSION["username"])) {
        require_once "model/computerManagement.php";
        $computers = getExampleBuilds();
        require "view/userBuilderMenu.php";
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the page to create a new component
 * @param $arrayofInputs array
 * @param $category string
 */
function createComponent($arrayofInputs, $category,$id)
{
    if (isset($_SESSION["username"])) {
        if ($_SESSION["userType"] == "admin") {
            if (isset($arrayofInputs["registerName"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerDescription"])) {
                unset($_SESSION["uploadImageError"]);
                $_SESSION["newComponentError"] = 0;
                $username = $_SESSION["username"];
                $createComponent = false;
                $uploadImageNotError = false;
                $image_path = null;
                $filename = null;
                switch ($category) {
                    case "cases":
                        if (isset($arrayofInputs["registerDescription"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerName"]) && isset($arrayofInputs["form_factor_motherboard"]) && isset($arrayofInputs["max_length_gpu"]) && isset($arrayofInputs["max_height_cpu"]) && isset($arrayofInputs["form_factor_power_supply"])) {

                            $name = strtolower($arrayofInputs["registerName"]);
                            $description = strtolower($arrayofInputs["registerDescription"]);
                            $price = (float)$arrayofInputs["registerPrice"];
                            $max_length_gpu = (int)$arrayofInputs["max_length_gpu"];
                            $max_height_cpu = (int)$arrayofInputs["max_height_cpu"];
                            $form_factor_motherboard = strtolower($arrayofInputs["form_factor_motherboard"]);
                            $form_factor_power_supply = strtolower($arrayofInputs["form_factor_power_supply"]);
                            $aio_water = $arrayofInputs["aio_water"];
                            $aio = false;

                            require_once "model/inventoryManagement.php";
                            require_once  "model/imageManagement.php";
                            //check if an image has been imported
                            if ($_FILES["imgUpload"]["name"] != "") {
                                require_once "model/imageManagement.php";
                                $filename = createUniqueFileName($username, $category);
                                $file_dir = "img/";
                                $image_path = $file_dir . $filename;
                            }
                            //check if the aio check box has been selected
                            if (isset($aio_water)) {
                                $aio = true;
                            }
                            //check if the filename isn't null
                            if ($filename != null) {
                                //upload the image if the filename is existent
                                $uploadImageNotError = isUploadImage($filename);
                                if($uploadImageNotError)
                                {
                                    $_SESSION["uploadImageError"] = 1;
                                }
                            }
                            //check if there has been an error uploading the image
                            if(!$uploadImageNotError)
                            {
                                //Create new component in data base
                                if (isCreateCasesInDB($id,$name, $description, $price, $image_path, $max_height_cpu, $max_length_gpu, $form_factor_motherboard,$form_factor_power_supply, $aio)) {
                                    $createComponent = true;
                                }
                            }

                        } else {
                            $_SESSION["newComponentError"] = 1;
                        }
                        break;
                    case "processors":
                        if (isset($arrayofInputs["registerDescription"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerName"]) && isset($arrayofInputs["socket_cpu"])) {
                            $name = strtolower($arrayofInputs["registerName"]);
                            $description = strtolower($arrayofInputs["registerDescription"]);
                            $price = (float)$arrayofInputs["registerPrice"];
                            $socket = strtolower($arrayofInputs["socket_cpu"]);

                            require_once "model/inventoryManagement.php";

                            if ($_FILES["imgUpload"]["name"] != "") {
                                require_once "model/imageManagement.php";
                                $filename = createUniqueFileName($username, $category);
                                $file_dir = "img/";
                                $image_path = $file_dir . $filename;
                            }
                            if ($filename != null) { //upload the image if the filename is existent
                                $uploadImageNotError = isUploadImage($filename);
                                if($uploadImageNotError)
                                {
                                    $_SESSION["uploadImageError"] = 1;
                                }
                            }
                            if(!$uploadImageNotError)
                            {
                                //Create new component in data base
                                if (IsCreateProcessorInDB($id,$name, $description, $price, $image_path, $socket)) {
                                    $createComponent = true;
                                }
                            }

                        } else {
                            $_SESSION["newComponentError"] = 1;
                        }
                        break;
                    case "coolers":
                        if (isset($arrayofInputs["registerDescription"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerName"]) && isset($arrayofInputs["socket_support"]) && isset($arrayofInputs["max_height"])) {
                            $name = strtolower($arrayofInputs["registerName"]);
                            $description = strtolower($arrayofInputs["registerDescription"]);
                            $price = (float)$arrayofInputs["registerPrice"];
                            $socket = strtolower($arrayofInputs["socket_support"]);
                            $height = (int)$arrayofInputs["max_height"];
                            $aio_water = $arrayofInputs["aio_water"];
                            $aio = false;

                            require_once "model/inventoryManagement.php";

                            if ($_FILES["imgUpload"]["name"] != "") { //create a file name if an image has been selected
                                require_once "model/imageManagement.php";
                                $filename = createUniqueFileName($username, $category);
                                $file_dir = "img/";
                                $image_path = $file_dir . $filename;
                            }
                            if (isset($aio_water)) {
                                $aio = true;
                            }
                            if ($filename != null) { //upload the image if the filename is existent
                                $uploadImageNotError = isUploadImage($filename);
                                if($uploadImageNotError)
                                {
                                    $_SESSION["uploadImageError"] = 1;
                                }
                            }
                            if(!$uploadImageNotError)
                            {
                                //Create new component in data base
                                if (IsCreateCoolersInDB($id,$name, $description, $price, $image_path, $socket, $height, $aio)) {
                                    $createComponent = true;
                                }
                            }

                        } else {
                            $_SESSION["newComponentError"] = 1;
                        }
                        break;
                    case "motherboard":
                        if (isset($arrayofInputs["registerDescription"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerName"]) && isset($arrayofInputs["socket_cpu"]) && isset($arrayofInputs["form_factor"]) && isset($arrayofInputs["form_factor_ram"]) && isset($arrayofInputs["interface_m2"])) {
                            $name = strtolower($arrayofInputs["registerName"]);
                            $description = strtolower($arrayofInputs["registerDescription"]);
                            $price = (float)$arrayofInputs["registerPrice"];
                            $socket = strtolower($arrayofInputs["socket_cpu"]);
                            $form_factor_ram = strtolower($arrayofInputs["form_factor_ram"]);
                            $form_factor = strtolower($arrayofInputs["form_factor"]);
                            $interface_m2 = strtolower($arrayofInputs["interface_m2"]);

                            require_once "model/inventoryManagement.php";

                            if ($_FILES["imgUpload"]["name"] != "") { //create a file name if an image has been selected
                                require_once "model/imageManagement.php";
                                $filename = createUniqueFileName($username, $category);
                                $file_dir = "img/";
                                $image_path = $file_dir . $filename;
                            }
                            if ($filename != null) { //upload the image if the filename is existent
                                $uploadImageNotError = isUploadImage($filename);
                                if($uploadImageNotError)
                                {
                                    $_SESSION["uploadImageError"] = 1;
                                }
                            }
                            if(!$uploadImageNotError)
                            {
                                //Create new component in data base
                                if (IsCreateMotherboardInDB($id,$name, $description, $price, $image_path, $socket, $form_factor_ram, $form_factor, $interface_m2)) {
                                    $createComponent = true;
                                }
                            }
                        } else {
                            $_SESSION["newComponentError"] = 1;
                        }
                        break;
                    case "graphics_cards":
                        if (isset($arrayofInputs["registerDescription"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerName"]) && isset($arrayofInputs["max_length_gpu"])) {
                            $name = strtolower($arrayofInputs["registerName"]);
                            $description = strtolower($arrayofInputs["registerDescription"]);
                            $price = (float)$arrayofInputs["registerPrice"];
                            $max_length_gpu = (int)$arrayofInputs["max_length_gpu"];

                            require_once "model/inventoryManagement.php";

                            if ($_FILES["imgUpload"]["name"] != "") { //create a file name if an image has been selected
                                require_once "model/imageManagement.php";
                                $filename = createUniqueFileName($username, $category);
                                $file_dir = "img/";
                                $image_path = $file_dir . $filename;
                            }
                            if ($filename != null) { //upload the image if the filename is existent
                                $uploadImageNotError = isUploadImage($filename);
                                if($uploadImageNotError)
                                {
                                    $_SESSION["uploadImageError"] = 1;
                                }
                            }
                            if(!$uploadImageNotError)
                            {
                                //Create new component in data base
                                if (IsCreateGraphicsCardInDB($id,$name, $description, $price, $image_path, $max_length_gpu)) {
                                    $createComponent = true;
                                }
                            }
                        } else {
                            $_SESSION["newComponentError"] = 1;
                        }
                        break;
                    case "ram":
                        if (isset($arrayofInputs["registerDescription"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerName"]) && isset($arrayofInputs["form_factor"])) {
                            $name = strtolower($arrayofInputs["registerName"]);
                            $description = strtolower($arrayofInputs["registerDescription"]);
                            $price = (float)$arrayofInputs["registerPrice"];
                            $form_factor = strtolower($arrayofInputs["form_factor"]);

                            require_once "model/inventoryManagement.php";

                            if ($_FILES["imgUpload"]["name"] != "") { //create a file name if an image has been selected
                                require_once "model/imageManagement.php";
                                $filename = createUniqueFileName($username, $category);
                                $file_dir = "img/";
                                $image_path = $file_dir . $filename;
                            }
                            if ($filename != null) { //upload the image if the filename is existent
                                $uploadImageNotError = isUploadImage($filename);
                                if($uploadImageNotError)
                                {
                                    $_SESSION["uploadImageError"] = 1;
                                }
                            }
                            if(!$uploadImageNotError)
                            {
                                //Create new component in data base
                                if (IsCreateRamInDB($id,$name, $description, $price, $image_path, $form_factor)) {
                                    $createComponent = true;
                                }
                            }
                        } else {
                            $_SESSION["newComponentError"] = 1;
                        }
                        break;
                    case "power_supply":
                        if (isset($arrayofInputs["registerDescription"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerName"]) && isset($arrayofInputs["form_factor"])) {
                            $name = strtolower($arrayofInputs["registerName"]);
                            $description = strtolower($arrayofInputs["registerDescription"]);
                            $price = (float)$arrayofInputs["registerPrice"];
                            $form_factor = strtolower($arrayofInputs["form_factor"]);

                            require_once "model/inventoryManagement.php";
                            require_once "model/imageManagement.php";
                            if ($_FILES["imgUpload"]["name"] != "") { //create a file name if an image has been selected
                                require_once "model/imageManagement.php";
                                $filename = createUniqueFileName($username, $category);
                                $file_dir = "img/";
                                $image_path = $file_dir . $filename;
                            }
                            if ($filename != null) { //upload the image if the filename is existent
                                $uploadImageNotError = isUploadImage($filename);
                                if($uploadImageNotError)
                                {
                                    $_SESSION["uploadImageError"] = 1;
                                }
                            }
                            if(!$uploadImageNotError)
                            {
                                //Create new component in data base
                                if (IsCreatePowerSupplyInDB($id,$name, $description, $price, $image_path, $form_factor)) {
                                    $createComponent = true;
                                }
                            }
                        } else {
                            $_SESSION["newComponentError"] = 1;
                        }
                        break;
                    case "storage":
                        if (isset($arrayofInputs["registerDescription"]) && isset($arrayofInputs["registerPrice"]) && isset($arrayofInputs["registerName"]) && isset($arrayofInputs["form_factor"]) && isset($arrayofInputs["interface_m2"])) {

                            $name = strtolower($arrayofInputs["registerName"]);
                            $description = strtolower($arrayofInputs["registerDescription"]);
                            $price = (float)$arrayofInputs["registerPrice"];
                            $form_factor = strtolower($arrayofInputs["form_factor"]);
                            $interface_m2 = strtolower($arrayofInputs["interface_m2"]);

                            require_once "model/inventoryManagement.php";

                            if ($_FILES["imgUpload"]["name"] != "") { //create a file name if an image has been selected
                                require_once "model/imageManagement.php";
                                $filename = createUniqueFileName($username, $category);
                                $file_dir = "img/";
                                $image_path = $file_dir . $filename;
                            }
                            if ($filename != null) { //upload the image if the filename is existent
                                $uploadImageNotError = isUploadImage($filename);
                                if($uploadImageNotError)
                                {
                                    $_SESSION["uploadImageError"] = 1;
                                }
                            }
                            if(!$uploadImageNotError)
                            {
                                //Create new component in data base
                                if (IsCreateStorageInDB($id,$name, $description, $price, $image_path, $form_factor, $interface_m2)) {
                                    $createComponent = true;
                                }
                            }
                        } else {
                            $_SESSION["newComponentError"] = 1;
                        }
                        break;
                }
                //check if there isn't an error to create component or image upload error
                if ($createComponent == true && $uploadImageNotError != true) {
                    $_GET["action"] = "adminInventory";
                    unset($_GET["category"]);
                    unset($_POST);
                    require "view/inventoryManager.php";
                } else {
                    //check if id isn't null
                    if((int)$id != null)
                    {
                        editComponent($category,$id); //display the form for editing a component
                    }else{
                        //error not all inputs have been completed
                        require_once "model/inventoryManagement.php";
                        $_SESSION["newComponentError"] = 1;
                        $results = getColumnNamesFromTableByCategory($category);
                        array_shift($results);
                        $properties = sortProperties($results);
                        $title = formatTitle($category);
                        require "view/createComponent.php";
                    }

                }
            } else {
                //check if id isn't null
                if((int)$id != null)
                {
                    editComponent($category,$id);//display the form for editing a component
                }else{
                    //error not all inputs have been completed
                    require_once "model/inventoryManagement.php";
                    $_SESSION["newComponentError"] = 1;
                    $results = getColumnNamesFromTableByCategory($category);
                    array_shift($results);
                    $properties = sortProperties($results);
                    $title = formatTitle($category);
                    require "view/createComponent.php";
                }
            }

        } else {
            home();
        }
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the list of components for a user to build a PC
 * @param $category string
 */
function selectComponent($category)
{
    if (isset($_SESSION["username"])) {
        $_GET["action"] = "selectComponent";
        require_once "model/inventoryManagement.php";
        $results = inventoryComponentList($category);
        $title = formatTitle($category);
        require "view/userInventoryComponentList.php";
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to add the selected part to the session like a cart and displays the build page
 * @param $category string
 * @param $id int
 */
function addSelectedComponent($category, $id)
{
    if (isset($_SESSION["username"])) {
        $id = (int)$id;
        require_once "model/inventoryManagement.php";
        if (IsSelectedItemExists($category, $id)) {
            $_SESSION["selected.$category"] = getComponentInfo($category, $id);
            if($_SESSION["createBuildType"] === 1)
            {
                require "view/modelBuilderSystem.php";
            }else{
                require "view/userBuilderSystem.php";
            }

        } else {
            userBuildMenu();
        }
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to remove a selected component from the session
 * @param $category string
 * @param $id int
 */
function deleteSelectedComponent($category,$id)
{
    if (isset($_SESSION["username"])) {
        unset($_SESSION["selected.$category"]);
        if($_SESSION["createBuildType"] === 1)
        {
            require "view/modelBuilderSystem.php";
        }else{
            require "view/userBuilderSystem.php";
        }

    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to remove a selected user from the data base
 * @param $username string
 */
function deleteUser($username)
{
    if ($_SESSION["userType"] == "admin") {
        unset($_SESSION["deleteUserError"]);
        require_once "model/userManagement.php";
        if (isDeleteUserCorrect($username)) {
            $_SESSION["deleteUserError"] = 0;
            userManager();
        } else {
            $_SESSION["deleteUserError"] = 1;
        }
    } else {
        home();
    }
}

/**
 * This function is designed to save a build to the data base
 * @param $type int
 */
function saveBuild($type)
{
    $type = (int)$type;
    $cases = $_SESSION["selected.cases"];
    $coolers = $_SESSION["selected.coolers"];
    $processor = $_SESSION["selected.processors"];
    $ram = $_SESSION["selected.ram"];
    $graphics_cards = $_SESSION["selected.graphics_cards"];
    $storage = $_SESSION["selected.storage"];
    $power_supply = $_SESSION["selected.power_supply"];
    $motherboard = $_SESSION["selected.motherboard"];
    $username = $_SESSION["username"];

    if (isset($_SESSION["username"]))
    {
        if($cases != null || $coolers != null || $processor != null || $ram != null || $graphics_cards != null || $storage != null || $power_supply != null || $motherboard != null)
        {
            if($type === 1 || $type === 0)
            {
                //set image null if it's not an example
                if($type === 0)
                {
                    $image = null;
                }
                if($type === 1)
                {
                    if(isset($_SESSION["userType"]) == "admin")
                    {
                        require_once "model/inventoryManagement.php";
                        require_once  "model/imageManagement.php";
                        $filename = null;
                        if ($_FILES["imgUpload"]["name"] != "") {
                            $category = "computers";
                            $filename = createUniqueFileName($username, $category);
                        }
                        if ($filename != null) {
                            if(isUploadImage($filename))
                            {
                                //image error
                            }else{
                                $file_dir = "img/";
                                $image =  $file_dir.$filename;
                            }
                        }
                    }
                }
            }else{
                $type = 0;
            }
            require_once "model/computerManagement.php";

            if (isset($_SESSION["selected.buildID"])) {
                if(isUpdateComputerInDB($username,$cases, $coolers, $processor, $ram, $graphics_cards, $storage, $power_supply, $motherboard, $image,$_SESSION["selected.buildID"]))
                {
                    unset($_SESSION["selected.buildID"]);
                    $_SESSION["updatePCError"] = 0;
                    if($type === 1 && $_SESSION["userType"] == "admin")
                    {
                        modelManager();
                    }else
                    {
                        myBuilds();
                    }
                }else{
                    $_SESSION["updatePCError"] = 1;
                    $buildID = $_SESSION["selected.buildID"];
                    unset($_SESSION["selected.buildID"]);
                    createBuild("example",$buildID);

                }
            } elseif (isset($_SESSION["selected.cases"]) || isset($_SESSION["selected.coolers"]) || isset($_SESSION["selected.motherboard"]) || isset($_SESSION["selected.processors"]) || isset($_SESSION["selected.storage"]) || isset($_SESSION["selected.graphics_cards"]) || isset($_SESSION["selected.ram"]) || isset($_SESSION["selected.power_supply"])) {
                if (isCreateComputerInDB($username, $cases, $coolers, $processor, $ram, $graphics_cards, $storage, $power_supply, $motherboard, $image, $type)) {
                    $_SESSION["createPCError"] = 0;
                    if($type === 1 && $_SESSION["userType"] == "admin")
                    {
                        modelManager();
                    }else
                    {
                        myBuilds();
                    }
                }else{
                    $_SESSION["createPCError"] = 1;
                    if($type === 1 && $_SESSION["userType"] == "admin")
                    {
                        modelManager();
                    }else
                    {
                        myBuilds();
                    }
                }
            }
        }else{
            $_SESSION["createPCError"] = 1;
            if($type === 1 && $_SESSION["userType"] == "admin")
            {
                modelManager();
            }else
            {
                myBuilds();
            }
        }
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the PDF of the selected components
 * @throws \Mpdf\MpdfException
 */
function exportPdf()
{
    if (isset($_SESSION["username"])) {
        require_once "model/exportToPDF.php";
        $cases = $_SESSION["selected.cases"];
        $coolers = $_SESSION["selected.coolers"];
        $processor = $_SESSION["selected.processors"];
        $ram = $_SESSION["selected.ram"];
        $graphics_cards = $_SESSION["selected.graphics_cards"];
        $storage = $_SESSION["selected.storage"];
        $power_supply = $_SESSION["selected.power_supply"];
        $motherboard = $_SESSION["selected.motherboard"];
        exportHTMlToPDF($cases,$coolers,$processor,$ram,$graphics_cards,$storage,$power_supply,$motherboard);
    } else {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the list of builds of a user
 */
function myBuilds()
{
    if (isset($_SESSION["username"])) {
        require_once "model/computerManagement.php";
        $results = getUserBuilds($_SESSION["username"]);
        require "view/userBuilds.php";
    }
}

/**
 * This function is designed  to delete a selected build by it's owner
 * @param $type int
 * @param $computerID int
 */
function deleteBuild($type,$computerID)
{
    if(isset($_SESSION["username"]))
    {
        unset($_SESSION["deleteComputerError"]);
        require_once "model/computerManagement.php";
        $type = (int)$type;
        if($type === 1 && $_SESSION["userType"] == "admin")
        {
            if(isDeleteExampleBuildCorrect($computerID)){
                $_SESSION["deleteComputerError"] = 0;
                modelManager();
            }else{
                $_SESSION["deleteComputerError"] = 1;
                modelManager();
            }
        }else
        {
            if(isDeleteBuildCorrect($_SESSION["username"],$computerID)){
                $_SESSION["deleteComputerError"] = 0;
                myBuilds();
            }else{
                $_SESSION["deleteComputerError"] = 1;
                myBuilds();
            }
        }


    }else{
        require "view/login.php";
    }
}
/**
 * This function is designed to activate and deactivate a component
 */
function visibilityComponent($category,$id)
{
    if(isset($_SESSION["username"]))
    {
        if($_SESSION["userType"] == "admin")
        {
            require "model/inventoryManagement.php";
            if(isChangedVisibilityState($category,$id)){
                $_SESSION["statusComponent"] = 0;
                inventoryManager();
            }else{
                $_SESSION["statusComponent"] = 0;
                inventoryManager();
            }
        }else{
            componentList($category);
        }
    }else{
        require "view/login.php";
    }
}

/**
 * This function is designed to edit a selected component
 * @param $category string
 * @param $id int
 */
function editComponent($category,$id)
{
    if(isset($_SESSION["username"]))
    {
        if($_SESSION["userType"] == "admin")
        {
            require_once "model/inventoryManagement.php";
            $_SESSION["selected.componentID"] = $id;
            $component = getComponentInfo($category,$id);
            $results = getColumnNamesFromTableByCategory($category);
            array_shift($results);
            $properties = sortProperties($results);
            $title = formatTitle($category);
            require "view/adminInventoryComponentEdit.php";
        }
    }else{
        require "view/login.php";
    }
}

/**
 * This function is designed to create a cookie to pass to javascript and go through the compatibility test
 * @param $storage
 * @param $power_supply
 * @param $cases
 * @param $ram
 * @param $motherboards
 * @param $processors
 * @param $graphics_cards
 * @param $coolers
 */
function formatForCookie($storage,$power_supply,$cases,$ram,$motherboards,$processors,$graphics_cards,$coolers){
    $listofComponents = [];
    $cookieName = "username";

    if($storage != null)
    {
        array_push($listofComponents,$storage);
    }
    if($power_supply != null)
    {
        array_push($listofComponents,$power_supply);
    }
    if($cases != null)
    {
        array_push($listofComponents,$cases);
    }
    if($ram != null)
    {
        array_push($listofComponents,$ram);
    }
    if($motherboards != null)
    {
        array_push($listofComponents,$motherboards);
    }
    if($processors != null)
    {
        array_push($listofComponents,$processors);
    }
    if($graphics_cards != null)
    {
        array_push($listofComponents,$graphics_cards);
    }
    if($coolers != null)
    {
        array_push($listofComponents,$coolers);
    }
    if(count($listofComponents) !== 0)
    {
        createCookie($_SESSION['username'],$cookieName);
        require_once "model/fileManagement.php";
        createTempFile($_SESSION['username'],json_encode($listofComponents));
    }
}

/**
 * This function is designed to set a cookie with a value and name
 * @param $value string
 * @param $name string
 */
function createCookie($value,$name)
{
    setcookie($name,$value);
}
