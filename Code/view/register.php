<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file is the view of the register page to create a account
 */
ob_start();
$titre="Register";
?>

<div class="font-sans antialiased bg-grey-lightest">
    <!-- Content -->
    <form class = "" method="POST" action="index.php?action=register">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-5/6 max-w-lg mx-auto bg-gray-900 rounded shadow-2xl">
                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Register now</div>
                    <div class="py-4 px-8">
                        <div class="flex mb-4"></div>
                        <div class="grid grid-cols-2">
                            <div class="mb-4 mx-1">
                                <div class="text-sm font-bold text-gray-100 tracking-wide">First name</div>
                                <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="text" placeholder="First name" name="registerUserFirstname" required>
                            </div>
                            <div class="mb-4 mx-1">
                                <div class="text-sm font-bold text-gray-100 tracking-wide">Last name</div>
                                <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="text" placeholder="Last name" name="registerUserLastname"required>
                            </div>
                        </div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Email Address</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="email" placeholder="Enter your email address" name="registerUserEmailAddress" required>
                        </div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Username</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="text" placeholder="Enter your username" name="registerUserUsername" required>
                        </div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">
                                Password
                            </div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="password" placeholder="Enter your password" name="registerUserPassword" required>
                        </div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">
                                Confirm password
                            </div>
                            <input class="w-full text-lg px-6 py-2  border-b border-gray-300 focus:outline-none focus:border-yellow-500 border-4 rounded-2xl" type="password" placeholder="Confirm your password" name="registerRepeatUserPassword">
                            <?php if(isset($_SESSION["registerError"])){
                                echo "<p class='text-red-600 text-xs font-bold mt-1'>E-mail, username already exists</p>";
                            }?>
                            <?php if(isset($_SESSION["registerPasswordError"])){
                                echo "<p class='text-red-600 text-xs font-bold mt-1'>Passwords aren't the same</p>";
                            }?>
                        </div>
                        <div class="flex items-center justify-between mt-8">
                            <button type="submit" class="bg-yellow-600 text-gray-100 p-4 w-full rounded-2xl tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:yellow-400
                                        shadow-lg">
                                Sign up
                            </button>
                        </div>
                        <div>
                            <p class="text-center my-4">
                                <a href="index.php?action=login" class="text-white font-bold text-sm no-underline hover:text-yellow-500">I already have an account? Sign in</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<?php
$contenu = ob_get_clean();
require "gabarit.php";

?>



