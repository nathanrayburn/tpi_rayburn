<?php
/**
 * Author : Nathan Rayburn
 * Date : 10/05/2021
 * Description : This file is the body for all view pages
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title><?=$titre;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Html5TemplatesDreamweaver.com">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> <!-- Remove this Robots Meta Tag, to allow indexing of site -->
    <link href="css/tailwind.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/gabarit.js"></script>
</head>
<body id="pageBody" class="bg-gray-800">

<!-- Top Nav -->
<div class="w-full bg-gray-900 fixed shadow-2xl z-1">
    <div class="container mx-auto">
        <div class="w-full flex justify-between items-center py-4 px-8">
            <!-- Brand -->
            <div class="text-white font-bold">Tower Part Selector</div>

            <!-- Navigation -->
                <nav class="relative xl:max-h-10 lg:max-h-10 pt-2 md:pt-1 pb-1 px-1 mt-0 h-auto fixed w-full z-20 top-0 flex flex-col md:flex-row md:space-x-4 font-semibold w-full md:w-auto bg-gray-900 shadow-md rounded-lg md:rounded-none md:shadow-none md:bg-transparent">
                    <a id="hamburger" class="animate-pulse my-auto border-2 border-white hover:border-gray-400 cursor-pointer hover:border-transparent rounded">
                        <svg xmlns="http://www.w3.org/2000/svg" class="toggle hidden h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="white">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 19l-7-7 7-7m8 14l-7-7 7-7" />
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" class="toggle block h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="white">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 5l7 7-7 7M5 5l7 7-7 7" />
                        </svg>
                    </a>
                <?php if($titre !=  "Home") :?>
                    <a  id="buttonHome" href="index.php?action=home" class="toggle transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-gray-300 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">
                        Home
                    </a>
                <?php endif?>
                <?php if (isset($_SESSION["username"])) :?>
                    <a id="buttonBuild" href="index.php?action=userBuildMenu" class="toggle transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-gray-300 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">
                        Build a PC
                    </a>
                    <a id="buttonLoad" href="index.php?action=mybuilds" class="toggle transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-gray-300 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">
                        My builds
                    </a>
                    <a id="buttonProfile" href="index.php?action=profile" class="toggle transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-gray-300 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">
                        My Profile
                    </a>
                        <?php if($_SESSION["userType"] == "admin") :?>
                            <a id="buttonAdmin" href="index.php?action=adminUser" class="toggle transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-red-500 text-white font-semibold hover:text-gray-800  px-4 border border-white hover:border-transparent rounded">
                                Admin Panel
                            </a>
                        <?php endif?>
                    <a>
                        <a  id="buttonLogout" href="index.php?action=logout" class="toggle transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-yellow-600 text-white font-semibold hover:text-gray-800  px-4 border border-white hover:border-transparent rounded">
                            Logout
                        </a>
                    </a>
                <?php  else:?>
                            <a id="buttonSignIn" href="index.php?action=login" class="toggle transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-gray-300 text-white font-semibold hover:text-gray-800  px-4 border border-white hover:border-transparent rounded">
                                Sign in
                            </a>
                            <a id="buttonSignUp" href="index.php?action=register" class="toggle transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-gray-300 text-white font-semibold hover:text-gray-800  px-4 border border-white hover:border-transparent rounded">
                                Sign up
                            </a>
                <?php endif ?>
            </nav>
        </div>
    </div>
</div>
<div class="">

    <div class="divPanel notop page-content">
        <div class="row-fluid">

            <!--__________CONTENU__________-->

            <div class="" id="divMain">
                <?=$contenu;?>
            </div>

            <!--________FIN CONTENU________-->

        </div>

        <div id="footerInnerSeparator"></div>
    </div>
</div>

<!-- Footer -->
<footer class="w-full bg-grey-lighter py-8">
    <div class="container mx-auto text-center px-8">
        <p class="text-white underline mb-2 text-sm">This is a product of <span class="font-bold">Nathan Rayburn</span></p>
    </div>
</footer>

</body>
</html>