<?php
/**
 * Author : Nathan Rayburn
 * Date : 25/05/2021
 * Description : This file is designed to be the view of the user's list of components
 */
ob_start();
?>
    <section class="text-gray-600 body-font">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-full mx-auto">
                    <?php
                        echo '                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">List of components - '.ucfirst($title).'</div>';
                    ?>
                    <div class="py-4 px-8 place-items-center">
                        <div class="">
                            <div class="container">
                                <table class="w-full flex flex-row flex-no-wrap rounded-lg overflow-hidden sm:shadow-lg my-5">
                                    <thead class="text-white">
                                    <?php
                                        $numberOfResults = count($results);
                                        for($i = 0;$i<$numberOfResults;$i++):?>
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Action</th>
                                        </tr>
                                    <?php endfor;?>
                                    </thead>
                                    <tbody class="flex-1 sm:flex-none text-white font-bold">
                                    <?php
                                        foreach($results as $component):?>
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="
                                                <?php
                                                    if($component["image"] == ""){
                                                        echo "img/defaultImage.jpg";
                                                    }else{
                                                        echo $component["image"];
                                                    }
                                                ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php echo $component["name"]?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php echo $component["price"]?> CHF</td>
                                        <td class="p-3 text-green-400 hover:text-green-600 hover:font-medium cursor-pointer"><a href="index.php?action=addSelectedComponent&category=<?php echo $category; ?>&id=<?php echo $component[0]?>">Select</a></td>
                                         </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <style>
                                @media (min-width: 640px) {
                                    table {
                                        display: inline-table !important;
                                    }

                                    thead tr:not(:first-child) {
                                        display: none;
                                    }
                                }

                                td:not(:last-child) {
                                    border-bottom: 0;
                                }

                                th:not(:last-child) {
                                    border-bottom: 0px solid rgba(0, 0, 0, .1);
                                }
                            </style>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
$content = ob_get_clean();
require "userBuilder.php";
?>