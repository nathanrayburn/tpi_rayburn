<?php
/**
 * Author : Nathan Rayburn
 * Date : 17/05/2021
 * Description : This file is designed to be the view of the user's profile
 */
ob_start();
$user = unserialize($_SESSION['userInformation']);
?>

<div class="flex flex-col place-items-center font-bold">
    <?php
    if($_SESSION["passwordError"] === 0 || $_SESSION["updateError"] === 0){
        echo '<div class="bg-green-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update has been successful</p>
    </div>
  </div>
</div>';
        unset($_SESSION["passwordError"]);
        unset($_SESSION["updateError"]);
    }

    ?>
    <div class="py-4 px-8 text-white font-mono text-3xl">My information</div>
    <div class="p-5 bg-gray-800 rounded-2xl">
        <div class="text-white">Username : <?php echo $user->get_username() ?></div>
        <div class="text-white">First name : <?php echo $user->get_firstname() ?></div>
        <div class="text-white">Last name : <?php echo $user->get_lastname() ?></div>
        <div class="text-white">E-mail : <?php echo $user->get_email() ?></div>
    </div>
    <div class="flex flex-col items-center mt-8 rounded-2xl">
        <a href="index.php?action=userPassword" class="m-2 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-yellow-600 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">change my password</a>
        <a href="index.php?action=profileInformation" class="m-2 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-center bg-transparent hover:bg-blue-400 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">update my personal information</a>
    </div>
</div>

<?php
$content = ob_get_clean();
require "userProfile.php";
?>
