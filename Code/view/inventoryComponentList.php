<?php
/**
 * Author : Nathan Rayburn
 * Date : 11/05/2021
 * Description : This file is designed to be the view of the admin's list of components to edit
 */
ob_start();
?>
    <section class="text-gray-600 body-font">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-full mx-auto">
                    <?php
                        echo '                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Inventory manager - '.ucfirst($title).'</div>';
                    ?>
                    <div class="py-4 px-8 place-items-center">
                        <div class="flex flex-row">
                            <a  href="index.php?action=createComponent&category=<?php echo $category;?>" class="m-2 text-center bg-transparent hover:bg-blue-400 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">add a new component</a>
                        </div>
                        <div class="container">
                            <table class="w-full flex flex-row flex-no-wrap rounded-lg overflow-hidden sm:shadow-lg my-5">
                                <thead class="text-white">
                                <?php
                                $numberOfResults = count($results);
                                for($i = 0;$i<$numberOfResults;$i++):?>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Edit</th>
                                        <th class="p-3 text-left">Status</th>
                                    </tr>
                                <?php endfor;?>
                                </thead>
                                <tbody class="flex-1 sm:flex-none text-white font-bold">
                                <?php
                                foreach($results as $component):?>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3"><?php echo $component["name"]?></td>
                                        <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer"><a href="index.php?action=editComponent&category=<?php echo $category; ?>&id=<?php echo $component[0];?>">Edit</a></td>
                                        <?php
                                            if($component["public_visibility"] == 1)
                                            {
                                                echo '<td class="p-3 text-red-400 hover:text-red-600 hover:font-medium cursor-pointer"><a href="index.php?action=visibilityComponent&category='.$category.'&id='.$component[0].'">Deactivate</a></td>';
                                            }else{
                                                echo '<td class="p-3 text-green-400 hover:text-green-600 hover:font-medium cursor-pointer"><a href="index.php?action=visibilityComponent&category='.$category.'&id='.$component[0].'">Activate</a></td>';
                                            }
                                        ?>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <style>
                            @media (min-width: 640px) {
                                table {
                                    display: inline-table !important;
                                }

                                thead tr:not(:first-child) {
                                    display: none;
                                }
                            }

                            td:not(:last-child) {
                                border-bottom: 0;
                            }

                            th:not(:last-child) {
                                border-bottom: 0px solid rgba(0, 0, 0, .1);
                            }
                        </style>
                </div>
            </div>
        </div>
    </section>
<?php
$content = ob_get_clean();
require "admin.php";
?>