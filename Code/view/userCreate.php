<?php
/**
 * Author : Nathan Rayburn
 * Date : 17/05/2021
 * Description : This file is designed to be the view of the admin's user creator
 */
ob_start();

?>
<!-- Content -->
<form class = "" method="POST" action="index.php?action=createNewUser">
    <div class="w-full" style="padding-top: 4rem;">
        <div class="container mx-auto py-8">
            <div class="">
                <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Create a new user</div>
                <div class="py-4 px-8 bg-gray-800 rounded-2xl">
                    <div class="grid grid-cols-2">
                        <div class="mb-4 mx-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">First name</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="text" placeholder="First name" name="registerUserFirstname" required>
                        </div>
                        <div class="mb-4 mx-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Last name</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="text" placeholder="Last name" name="registerUserLastname" required>
                        </div>
                    </div>
                    <div>
                        <div class="mb-4 mx-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Email Address</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="email" placeholder="Enter your email address" name="registerUserEmailAddress" required>
                        </div>
                    </div>
                    <div class="grid grid-cols-2">

                        <div class="mb-4 mx-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Username</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="text" placeholder="Enter your username" name="registerUserUsername" required>
                            <?php if(isset($_SESSION["createError"])){
                                echo "<p class='text-red-600 text-xs font-bold mt-1'>E-mail or username already exist</p>";
                            }?>
                        </div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">
                                Password
                            </div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="password" placeholder="Enter the user's password" name="registerUserPassword" required>
                        </div>
                    </div>
                    <div>

                    </div>
                    <div class="flex flex-col md:flex-row lg:flex-row xl:flex-row items-center mt-8">
                        <div class="md:w-2/12 lg:w-2/12 xl:w-2/12 m-2">
                            <a href="index.php?action=adminUser" class="bg-red-700 text-gray-100 p-4 w-full rounded-2xl tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-red-400
                                        shadow-lg text-center">Cancel</a>
                        </div>
                        <div class="w-full md:w-10/12 lg:md:w-10/12  xl:md:w-10/12 m-2">
                            <button type="submit" class="bg-green-600 text-gray-100 w-full p-4 rounded-2xl tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-400
                                        shadow-lg">
                                Create
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
$content = ob_get_clean();
require "admin.php";
?>
