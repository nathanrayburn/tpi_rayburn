<?php
/**
 * Author : Nathan Rayburn
 * Date : 11/05/2021
 * Description : This file is designed to be the view of the user's PC builder menu
 */
ob_start();
$titre="PC Builder";
?>
    <section class="text-gray-600 body-font">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-full mx-auto">
                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Build a system</div>
                    <div class="py-4 px-8 place-items-center">
                        <div class="m-2 flex flex-row place-items-center">
                            <a  href="index.php?action=userCreateBuild&computerType=0" class="m-2 text-center bg-transparent hover:bg-blue-400 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">create a new build</a>
                            <div class="m-2 text-white font-mono font-bold">Start come an example or create a new build!</div>
                        </div>
                        <div class="container">
                            <table class="w-full flex flex-row flex-no-wrap rounded-lg overflow-hidden sm:shadow-lg my-5">
                                <thead class="text-white">
                                <?php
                                $numberOfResults = count($computers);
                                for($i = 0;$i<$numberOfResults;$i++):?>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Example</th>
                                        <th class="p-3 text-left">Created date</th>
                                        <th class="p-3 text-left">Select</th>
                                    </tr>
                                <?php endfor;?>
                                </thead>
                                <tbody class="flex-1 sm:flex-none text-white font-bold">
                                <?php $i = 0;
                                foreach($computers as $computer): $i ++;?>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($computer["image"] == "") { echo "img/defaultImage.jpg";}else{echo $computer["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3">Example <?php echo $i;?></div>
                                        </td>
                                        <td><?php echo $computer["registration_date"]; ?></td>
                                        <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=userCreateBuild&computerType=0&computerID=<?php echo $computer[0]?>">Select</a></td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <style>
                            @media (min-width: 640px) {
                                table {
                                    display: inline-table !important;
                                }

                                thead tr:not(:first-child) {
                                    display: none;
                                }
                            }

                            td:not(:last-child) {
                                border-bottom: 0;
                            }

                            th:not(:last-child) {
                                border-bottom: 0px solid rgba(0, 0, 0, .1);
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
<?php
$content = ob_get_clean();
require "userBuilder.php";
?>