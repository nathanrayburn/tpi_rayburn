<?php
/**
 * Author : Nathan Rayburn
 * Date : 11/05/2021
 * Description : This file is designed to be the view of the admin's inventory manager
 */
ob_start();
$titre="Inventory Manager";
?>
    <section class="text-gray-600 body-font">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-full max-w-lg mx-auto">
                    <?php
                    if($_SESSION["newComponentError"] === 0 || $_SESSION["statusComponent"] === 0){
                        echo '<div class="bg-green-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update was successful</p>
    </div>
  </div>
</div>';
                    }elseif($_SESSION["newComponentError"] === 1){
                        echo '<div class="bg-red-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update was not successful</p>
    </div>
  </div>
</div>';
                    }
                    unset($_SESSION["newComponentError"]);
                    unset($_SESSION["statusComponent"]);
                    ?>
                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Inventory manager</div>
                    <div class="container">
                        <table class="w-full flex flex-row flex-no-wrap rounded-lg overflow-hidden sm:shadow-lg my-5">
                            <thead class="text-white">
                            <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                <th class="p-3 text-left">Category</th>
                                <th class="p-3 text-left">Action</th>
                            </tr>
                            <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                <th class="p-3 text-left">Category</th>
                                <th class="p-3 text-left">Action</th>
                            </tr>
                            <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                <th class="p-3 text-left">Category</th>
                                <th class="p-3 text-left">Action</th>
                            </tr>
                            <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                <th class="p-3 text-left">Category</th>
                                <th class="p-3 text-left">Action</th>
                            </tr>
                            <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                <th class="p-3 text-left">Category</th>
                                <th class="p-3 text-left">Action</th>
                            </tr>
                            <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                <th class="p-3 text-left">Category</th>
                                <th class="p-3 text-left">Action</th>
                            </tr>
                            <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                <th class="p-3 text-left">Category</th>
                                <th class="p-3 text-left">Action</th>
                            </tr>
                            <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                <th class="p-3 text-left">Category</th>
                                <th class="p-3 text-left">Action</th>
                            </tr>
                            </thead>
                            <tbody class="flex-1 sm:flex-none text-white font-bold">
                            <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                <td class="p-3">Cases</td>
                                <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=componentList&category=cases">Go to</a></td>
                            </tr>
                            <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                <td class="p-3">Processors</td>
                                <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=componentList&category=processors">Go to</a></td>
                            </tr>
                            <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                <td class="p-3">Coolers</td>
                                <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=componentList&category=coolers">Go to</a></td>
                            </tr>
                            <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                <td class="p-3">Motherboards</td>
                                <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=componentList&category=motherboard">Go to</a></td>
                            </tr>
                            <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                <td class="p-3">Video card</td>
                                <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=componentList&category=graphics_cards">Go to</a></td>
                            </tr>
                            <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                <td class="p-3">Memory</td>
                                <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=componentList&category=ram">Go to</a></td>
                            </tr>
                            <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                <td class="p-3">Power Supply</td>
                                <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=componentList&category=power_supply">Go to</a></td>
                            </tr>
                            <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                <td class="p-3">Storage</td>
                                <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=componentList&category=storage">Go to</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <style>
                        @media (min-width: 640px) {
                            table {
                                display: inline-table !important;
                            }

                            thead tr:not(:first-child) {
                                display: none;
                            }
                        }

                        td:not(:last-child) {
                            border-bottom: 0;
                        }

                        th:not(:last-child) {
                            border-bottom: 0px solid rgba(0, 0, 0, .1);
                        }
                    </style>
                </div>
                </div>
            </div>
        </div>
    </section>
<?php
$content = ob_get_clean();
require "admin.php";
?>