<?php
/**
 * Author : Nathan Rayburn
 * Date : 17/05/2021
 * Description : This file is designed to be the view of the user's password editor
 */
ob_start();
?>
<!-- Content -->
<form class="" method="POST" action="index.php?action=userPassword">
    <div class="w-full" style="padding-top: 4rem;">
        <div class="container mx-auto py-8">
            <div class="">
                <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Change my password</div>
                <div class="py-4 px-8 bg-gray-800 rounded-2xl">
                    <div class="mb-4">
                        <div class="mb-4 mx-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Current password</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="password" placeholder="Enter your current password" name="currentPassword"  required>
                        </div>

                    </div>
                    <div class="grid grid-cols-2">
                        <div class="m-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">New password</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="password" placeholder="Enter your new password" name="newPassword" required>
                            <?php if($_SESSION["passwordError"] == 1){
                                echo "<p class='text-red-600 text-xs font-bold mt-1'>Check if you have entered your passwords correctly</p>";
                            }?>
                        </div>
                        <div class="m-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Confirm new password</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="password" placeholder="Confirm your new password" name="repeatedNewPassword" required>
                        </div>

                    </div>
                    <div class="flex flex-col md:flex-row lg:flex-row xl:flex-row items-center mt-8">
                        <div class="md:w-2/12 lg:w-2/12 xl:w-2/12 m-2">
                            <a href="index.php?action=profile" class="bg-red-700 text-gray-100 p-4 w-full rounded-2xl tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-red-400
                                        shadow-lg text-center">Cancel</a>
                        </div>
                        <div class="w-full md:w-10/12 lg:md:w-10/12  xl:md:w-10/12 m-2">
                            <button type="submit" class="bg-yellow-600 text-gray-100 w-full p-4 rounded-2xl tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-yellow-400
                                        shadow-lg">
                                change password
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
$content = ob_get_clean();
require "userProfile.php";
?>
