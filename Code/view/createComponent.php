<?php
/**
 * Author : Nathan Rayburn
 * Date : 17/05/2021
 * Description : This file is designed to be the view of the admin's component creator
 */
ob_start();
?>
<!-- Content -->
<form method="POST" action="index.php?action=createComponent&category=<?php echo $category;?>" enctype="multipart/form-data">
    <div class="w-full" style="padding-top: 4rem;">
        <div class="container mx-auto py-8">
            <?php
            if($_SESSION["newComponentError"] === 0){
                echo '<div class="bg-green-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update was successful</p>
    </div>
  </div>
</div>';
            }elseif($_SESSION["newComponentError"] === 1){
                echo '<div class="bg-red-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update was not successful</p>
    </div>
  </div>
</div>';
            }
            unset($_SESSION["newComponentError"]);
            ?>
            <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Create a new <?php echo ucfirst($title);?></div>
                <div class="py-4 px-8 bg-gray-800 rounded-2xl">
                    <div class="grid grid-cols-2">
                        <div class="mb-4 mx-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Name</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="text" placeholder="Name" name="registerName" required>
                        </div>
                        <div class="mb-4 mx-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Price</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="number" placeholder="" name="registerPrice" required>
                        </div>
                    </div>
                    <div>
                        <div class="mb-4 mx-1">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Description</div>
                            <textarea class="w-full px-3 py-2 text-gray-700 border rounded-lg focus:outline-none" rows="4" name="registerDescription" placeholder="Description" required></textarea>
                        </div>
                    </div>
                    <?php
                        foreach($properties as $attribute)
                        {
                            $implode = explode("_",$attribute["Field"]);
                            $properyName = ucfirst(implode(" ",$implode));
                            $inputName = $attribute["Field"];
                            if($attribute["Null"] == "YES"){
                                $required = "";
                            }else{
                                $required = "required";
                            }
                            switch($attribute["Type"])
                            {
                                case "varchar(255)":
                                    $inputType = "text";
                                    echo '<div><div class="mb-4 mx-1"><div class="text-sm font-bold text-gray-100 tracking-wide">'.$properyName.'</div><input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="'.$inputType.'" placeholder="" name="'.$inputName.'" '.$required.'></div></div>';
                                    break;
                                case "int(11)":
                                    $inputType = "number";
                                    echo '<div><div class="mb-4 mx-1"><div class="text-sm font-bold text-gray-100 tracking-wide">'.$properyName.'</div><input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-2xl" type="'.$inputType.'" placeholder="" name="'.$inputName.'" '.$required.'></div></div>';
                                    break;
                                case "tinyint(4)":
                                    $inputType = "checkbox";
                                    echo '<div><div class="mb-4 mx-1"><div class="text-sm font-bold text-gray-100 tracking-wide">'.$properyName.'</div><input class="form-checkbox h-5 w-5 text-orange-600 rounded" type="'.$inputType.'" placeholder="" name="'.$inputName.'" '.$required.'></div></div>';
                                    break;
                            }

                        }
                    ?>
                    <div>
                        <div class="flex flex-col w-full items-center justify-center bg-grey-lighter">
                            <label class="w-64 flex flex-col items-center px-4 py-6 bg-yellow-600 text-blue rounded-lg shadow-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue hover:text-white">
                                <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                                </svg>
                                <span class="mt-2 text-base leading-normal">Select an image (only png,jpeg,jpg)</span>
                                <input name="imgUpload" type='file' class="hidden" onchange="readURL(this)"/>
                            </label>
                            <div>
                                <?php if($_SESSION["uploadImageError"] == 1): ?><p class="text-red-600 text-xs font-bold mt-1">Couldn't upload image, check extension or might be more than 500 KB</p>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row lg:flex-row xl:flex-row items-center mt-8">
                    <div class="md:w-2/12 lg:w-2/12 xl:w-2/12 m-2">
                        <a href="index.php?action=componentList&category=<?php echo $category; ?>" class="bg-red-700 text-gray-100 p-4 w-full rounded-2xl tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-red-400
                                        shadow-lg text-center">Cancel</a>
                    </div>
                    <div class="w-full md:w-10/12 lg:md:w-10/12  xl:md:w-10/12 m-2">
                        <button type="submit" class="bg-green-600 text-gray-100 w-full p-4 rounded-2xl tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-400
                                        shadow-lg">
                            Create
                        </button>
                    </div>
                </div>
        </div>
    </div>
    </div>
</form>
<?php
$content = ob_get_clean();
require "admin.php";
?>
