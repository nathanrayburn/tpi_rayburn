<?php
/**
 * Author : Nathan Rayburn
 * Date : 11/05/2021
 * Description : This file is designed to be the view of the admin's pc example manager
 */
ob_start();
$titre="Model manager";
?>
    <section class="text-gray-600 body-font">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-full max-w-lg mx-auto">
                    <?php
                    if($_SESSION["createPCError"] === 0){
                        echo '<div class="bg-green-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update was successful</p>
    </div>
  </div>
</div>';
                    }elseif($_SESSION["createPCError"] === 1){
                        echo '<div class="bg-red-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update was not successful</p>
    </div>
  </div>
</div>';
                    }
                    unset($_SESSION["createPCError"]);
                    ?>
                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">PC Model Manager</div>
                    <div class="py-4 px-8 place-items-center">
                        <div class="flex flex-row">
                            <a  href="index.php?action=userCreateBuild&computerType=1" class="m-2 text-center bg-transparent hover:bg-blue-400 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">create a new build</a>
                        </div>
                        <div class="container">
                            <table class="w-full flex flex-row flex-no-wrap rounded-lg overflow-hidden sm:shadow-lg my-5">
                                <thead class="text-white">
                                <?php
                                $numberOfResults = count($results);
                                for($i = 0;$i<$numberOfResults;$i++):?>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Date</th>
                                        <th class="p-3 text-left">Edit</th>
                                        <th class="p-3 text-left">Delete</th>
                                    </tr>
                                <?php endfor;?>
                                </thead>
                                <tbody class="flex-1 sm:flex-none text-white font-bold">
                                <?php
                                foreach($results as $build):?>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($build["image"] == "") { echo "img/defaultImage.jpg";}else{echo $build["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php echo $build["registration_date"]; ?></div>
                                        </td>
                                        <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer truncate"><a href="index.php?action=userCreateBuild&&computerType=1&computerID=<?php echo $build[0]?>">Edit</a></td>
                                        <td class="p-3 text-red-400 hover:text-red-600 hover:font-medium cursor-pointer"><a href="index.php?action=deleteSelectedBuild&computerType=1&computerID=<?php echo $build[0]?>">Delete</a></td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <style>
                            @media (min-width: 640px) {
                                table {
                                    display: inline-table !important;
                                }

                                thead tr:not(:first-child) {
                                    display: none;
                                }
                            }

                            td:not(:last-child) {
                                border-bottom: 0;
                            }

                            th:not(:last-child) {
                                border-bottom: 0px solid rgba(0, 0, 0, .1);
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
$content = ob_get_clean();
require "admin.php";
?>