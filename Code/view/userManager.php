<?php
/**
 * Author : Nathan Rayburn
 * Date : 11/05/2021
 * Description : This file is designed to be the view of the admin's user manager
 */
ob_start();
$titre="User manager";
?>
    <section class="text-gray-600 body-font">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-full mx-auto">
                    <?php
                    if($_SESSION["updateError"] === 0 || $_SESSION["createError"] === 0 || $_SESSION["deleteUserError"] === 0){
                        echo '<div class="bg-green-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update was successful</p>
    </div>
  </div>
</div>';

                    }elseif($_SESSION["updateError"] === 0 || $_SESSION["createError"] === 0 || $_SESSION["deleteUserError"] === 0){
                        echo '<div class="bg-red-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
  <div class="flex">
    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
    <div>
      <p class="text-white font-bold">The latest update was not successful</p>
    </div>
  </div>
</div>';
                    }
                    unset($_SESSION["updateError"]);
                    unset($_SESSION["createError"]);
                    unset($_SESSION["deleteUserError"]);
                    ?>
                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">User account manager</div>
                    <div class="py-4 px-8 place-items-center">
                        <div class="flex flex-row">
                            <a  href="index.php?action=createNewUser" class="m-2 text-center bg-transparent hover:bg-blue-400 text-white font-semibold hover:text-gray-800 px-4 border border-white hover:border-transparent rounded">create a new user</a>
                        </div>

                        <div class="container">
                            <table class="w-full flex flex-row flex-no-wrap rounded-lg overflow-hidden sm:shadow-lg my-5">
                                <thead class="text-white">
                                <?php

                                $numberOfResults = count($users);
                                for($i = 0;$i<$numberOfResults;$i++):?>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Edit</th>
                                        <th class="p-3 text-left">Delete</th>
                                    </tr>
                                <?php endfor;?>
                                </thead>
                                <tbody class="flex-1 sm:flex-none text-white font-bold">
                                <?php
                                foreach($users as $user):?>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3 text-blue-300"><?php echo $user->get_username()?></td>
                                        <td class="p-3 text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer"><a href="index.php?action=editUser&username=<?php echo $user->get_username(); ?>">Edit</a></td>
                                        <td class="p-3 text-red-400 hover:text-red-600 hover:font-medium cursor-pointer"><a href="index.php?action=deleteUser&username=<?php echo $user->get_username(); ?>">Delete</a></td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <style>
                            @media (min-width: 640px) {
                                table {
                                    display: inline-table !important;
                                }

                                thead tr:not(:first-child) {
                                    display: none;
                                }
                            }

                            td:not(:last-child) {
                                border-bottom: 0;
                            }

                            th:not(:last-child) {
                                border-bottom: 0px solid rgba(0, 0, 0, .1);
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
<?php
$content = ob_get_clean();
require "admin.php";
?>