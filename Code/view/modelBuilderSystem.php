<?php
/**
 * Author : Nathan Rayburn
 * Date : 31/05/2021
 * Description : This file is designed to be the view of the admin's PC example builder
 */
ob_start();
$titre="PC Builder";
?>
    <section class="text-gray-600 body-font">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <form  method="POST" action="index.php?action=saveBuild&computerType=1<?php if($computerID != null){echo "&computerID=$id";}?>" enctype="multipart/form-data">

                    <div class="w-full mx-auto">
                        <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Build a system</div>
                        <div class="py-4 px-8 place-items-center">
                            <div class="text-xs md:text-base lg:text-base xl:text-base">
                                <div>
                                    <div class="flex w-full items-center justify-center bg-grey-lighter">
                                        <label class="p-3 w-24 lg:w-40 xl:w-64 flex flex-col items-center px-4 py-6 bg-yellow-600 text-blue rounded-lg shadow-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue hover:text-white">
                                            <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                                            </svg>
                                            <span class="mt-2 md:text-xs lg:text-base xl:text-base">Select an image (only png,jpeg,jpg)</span>
                                            <input name="imgUpload" type='file' class="hidden" id="fileInput" onchange="readURL(this)"/>
                                        </label>
                                    </div>
                                    <div class="flex flex-row-reverse border-b-2 border-grey-dark overflow-hidden sm:items-center">
                                        <div class="flex flex-row-reverse rounded mx-1">
                                            <button type="submit" class="block text-white text-sm shadow-border bg-green-500 hover:bg-green-600 text-sm py-3 px-4 font-sans tracking-wide uppercase font-bold">
                                                    Save
                                                </button>
                                            <div class="bg-green-400 shadow-border p-3">
                                                <div class="w-4 h-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="white">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <table class="w-full flex flex-row flex-no-wrap rounded-lg overflow-hidden sm:shadow-lg my-5">
                                        <thead class="text-white">
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Category</th>
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Actions</th>
                                        </tr>
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Category</th>
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Actions</th>
                                        </tr>
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Category</th>
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Actions</th>
                                        </tr>
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Category</th>
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Actions</th>
                                        </tr>
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Category</th>
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Actions</th>
                                        </tr>
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Category</th>
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Actions</th>
                                        </tr>
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Category</th>
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Actions</th>
                                        </tr>
                                        <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                            <th class="p-3 text-left">Category</th>
                                            <th class="p-3 text-left">Name</th>
                                            <th class="p-3 text-left">Price</th>
                                            <th class="p-3 text-left">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody class="flex-1 sm:flex-none text-white font-bold">
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                            <td class="p-3">Cases</td>
                                            <td class="p-3 flex flex-row place-items-center">
                                                <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                    <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.cases"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.cases"][0]["image"];} ?>" alt="">
                                                </div>
                                                <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.cases"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.cases"][0]["name"];} ?></div>
                                            </td>
                                            <td class="p-3 truncate"><?php if($_SESSION["selected.cases"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.cases"][0]["price"]." CHF";} ?></td>
                                            <?php
                                            if(isset($_SESSION["selected.cases"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=cases">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=cases&id='.$_SESSION["selected.cases"][0][0].'">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=cases">Select</a></td>';
                                            }
                                            ?>
                                        </tr>
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                            <td class="p-3">Processor</td>
                                            <td class="p-3 flex flex-row place-items-center">
                                                <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                    <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.processors"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.processors"][0]["image"];} ?>" alt="">
                                                </div>
                                                <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.processors"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.processors"][0]["name"];} ?></div>
                                            </td>
                                            <td class="p-3 truncate"><?php if($_SESSION["selected.processors"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.processors"][0]["price"]." CHF";} ?></td>
                                            <?php
                                            if(isset($_SESSION["selected.processors"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=processors">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=processors&id='.$_SESSION["selected.processors"][0][0].'">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=processors">Select</a></td>';
                                            }
                                            ?>
                                        </tr>
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                            <td class="p-3">Cooler</td>
                                            <td class="p-3 flex flex-row place-items-center">
                                                <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                    <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.coolers"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.coolers"][0]["image"];} ?>" alt="">
                                                </div>
                                                <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.coolers"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.coolers"][0]["name"];} ?></div>
                                            </td>
                                            <td class="p-3 truncate"><?php if($_SESSION["selected.coolers"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.coolers"][0]["price"]." CHF";} ?></td>
                                            <?php
                                            if(isset($_SESSION["selected.coolers"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=coolers">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=coolers&id='.$_SESSION["selected.coolers"][0][0].'">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=coolers">Select</a></td>';
                                            }
                                            ?>
                                        </tr>
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                            <td class="p-3">Motherboard</td>
                                            <td class="p-3 flex flex-row place-items-center">
                                                <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                    <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.motherboard"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.motherboard"][0]["image"];} ?>" alt="">
                                                </div>
                                                <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.motherboard"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.motherboard"][0]["name"];} ?></div>
                                            </td>
                                            <td class="p-3 truncate"><?php if($_SESSION["selected.motherboard"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.motherboard"][0]["price"]." CHF";} ?></td>
                                            <?php
                                            if(isset($_SESSION["selected.motherboard"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=motherboard">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=motherboard&id='.$_SESSION["selected.motherboard"][0][0].'">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=motherboard">Select</a></td>';
                                            }
                                            ?>
                                        </tr>
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                            <td class="p-3">Graphics card</td>
                                            <td class="p-3 flex flex-row place-items-center">
                                                <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                    <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.graphics_cards"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.graphics_cards"][0]["image"];} ?>" alt="">
                                                </div>
                                                <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.graphics_cards"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.graphics_cards"][0]["name"];} ?></div>
                                            </td>
                                            <td class="p-3 truncate"><?php if($_SESSION["selected.graphics_cards"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.graphics_cards"][0]["price"]." CHF";} ?></td>
                                            <?php
                                            if(isset($_SESSION["selected.graphics_cards"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=graphics_cards">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=graphics_cards&id='.$_SESSION["selected.graphics_cards"][0][0].'">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=graphics_cards">Select</a></td>';
                                            }
                                            ?>
                                        </tr>
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                            <td class="p-3">Memory</td>
                                            <td class="p-3 flex flex-row place-items-center">
                                                <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                    <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.ram"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.ram"][0]["image"];} ?>" alt="">
                                                </div>
                                                <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.ram"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.ram"][0]["name"];} ?></div>
                                            </td>
                                            <td class="p-3 truncate"><?php if($_SESSION["selected.ram"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.ram"][0]["price"]." CHF";} ?></td>
                                            <?php
                                            if(isset($_SESSION["selected.ram"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=ram">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=ram&id='.$_SESSION["selected.ram"][0][0].'">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=ram">Select</a></td>';
                                            }
                                            ?>
                                        </tr>
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                            <td class="p-3">Power Supply</td>
                                            <td class="p-3 flex flex-row place-items-center">
                                                <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                    <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.power_supply"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.power_supply"][0]["image"];} ?>" alt="">
                                                </div>
                                                <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.power_supply"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.power_supply"][0]["name"];} ?></div>
                                            </td>
                                            <td class="p-3 truncate"><?php if($_SESSION["selected.power_supply"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.power_supply"][0]["price"]." CHF";} ?></td>
                                            <?php
                                            if(isset($_SESSION["selected.power_supply"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=power_supply">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=power_supply&id='.$_SESSION["selected.power_supply"][0][0].'">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=power_supply">Select</a></td>';
                                            }
                                            ?>
                                        </tr>
                                        <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                            <td class="p-3">Storage</td>
                                            <td class="p-3 flex flex-row place-items-center">
                                                <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                    <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.storage"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.storage"][0]["image"];} ?>" alt="">
                                                </div>
                                                <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.storage"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.storage"][0]["name"];} ?></div>
                                            </td>
                                            <td class="p-3 truncate"><?php if($_SESSION["selected.storage"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.storage"][0]["price"]." CHF";} ?></td>
                                            <?php
                                            if(isset($_SESSION["selected.storage"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=storage">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=storage&id='.$_SESSION["selected.storage"][0][0].'">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=storage">Select</a></td>';
                                            }
                                            ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <style>
                                    @media (min-width: 640px) {
                                        table {
                                            display: inline-table !important;
                                        }

                                        thead tr:not(:first-child) {
                                            display: none;
                                        }
                                    }

                                    td:not(:last-child) {
                                        border-bottom: 0;
                                    }

                                    th:not(:last-child) {
                                        border-bottom: 0px solid rgba(0, 0, 0, .1);
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
    </section>
<?php
$content = ob_get_clean();
require "admin.php";
?>