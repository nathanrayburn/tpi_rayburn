<?php
/**
 * Author : Nathan Rayburn
 * Date : 11/05/2021
 * Description : This file is designed to be canvas view of the admin panel
 */
ob_start();
$titre="Admin Panel";
?>

<body class="bg-gray-900 font-sans leading-normal tracking-normal mt-12">
<div class="flex flex-col md:flex-row">
    <div class="">
        <div class="h-16 md:h-12 lg:h-12 xl:h-12 md:mt-12 md:w-48 md:left-0 md:top-0 content-center md:content-start text-left justify-between">
            <ul class="list-reset flex flex-row md:flex-col py-0 md:py-3 px-1 md:px-2 text-center md:text-left">
                <li class="mr-3 flex-1">
                    <a href="index.php?action=adminUser" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800 hover:border-pink-500">
                        <i class="fas fa-tasks pr-0 md:pr-3"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 hover:text-white block md:inline-block">User manager</span>
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="index.php?action=adminModel" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800 hover:border-purple-500">
                        <i class="fa fa-envelope pr-0 md:pr-3"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 hover:text-white block md:inline-block">Example PC builder</span>
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="index.php?action=adminInventory" class="block py-1 md:py-3 pl-0 md:pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800 hover:border-red-500">
                        <i class="fa fa-wallet pr-0 md:pr-3"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 hover:text-white block md:inline-block">Inventory manager</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-content flex-1 bg-gray-600 xl:pt-12 md:pt-12 lg:pt-12 md:mt-2 pb-24 md:pb-5">
        <div class="bg-gray-900 pt-3">
            <div class="rounded-tl-3xl bg-gradient-to-r from-red-600 to-gray-800 p-4 shadow text-2xl text-white">
                <h3 class="font-bold pl-2">Admin panel</h3>
            </div>
        </div>
        <div class="w-full p-6">
            <!--Metric Card-->
            <div class="bg-gray-900 rounded shadow-2xl border-b-4 border-yellow-500 rounded-lg shadow-xl p-5">
                <?=$content;?>
            </div>
            <!--/Metric Card-->
        </div>
    </div>
</div>

</body>

<?php
$contenu = ob_get_clean();
require "gabarit.php";
?>
