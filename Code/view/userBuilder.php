<?php
/**
 * Author : Nathan Rayburn
 * Date : 11/05/2021
 * Description : This file is designed to be the canvas view of the PC builder panel
 */
ob_start();
$titre="PC Builder";
?>

<body class="bg-gray-900 font-sans leading-normal tracking-normal mt-12">
<div class="flex flex-col md:flex-row">
    <div class="main-content flex-1 bg-gray-600 pt-12 pt-12 pt-12 mt-2 pb-24 md:pb-5">
        <div class="bg-gray-900 pt-3">
            <div class="rounded-tl-3xl bg-gradient-to-r from-blue-600 to-gray-800 p-4 shadow text-2xl text-white">
                <h3 class="font-bold pl-2">PC Builder</h3>
            </div>
        </div>
        <div class="w-auto p-6">
            <!--Metric Card-->
            <div class="bg-gray-900 rounded shadow-2xl border-b-4 border-yellow-500 rounded-lg shadow-xl p-5">
                <?=$content;?>
            </div>
            <!--/Metric Card-->
        </div>
    </div>
</div>

</body>

<?php
$contenu = ob_get_clean();
require "gabarit.php";
?>
