<?php
/**
 * Author : Nathan Rayburn
 * Date : 11/05/2021
 * Description : This file is designed to be the view of the user's PC builder
 */
ob_start();
$titre="PC Builder";
//create cookie and temp json file
formatForCookie($_SESSION["selected.storage"],$_SESSION["selected.power_supply"],$_SESSION["selected.cases"],$_SESSION["selected.ram"],$_SESSION["selected.motherboard"],$_SESSION["selected.processors"],$_SESSION["selected.graphics_cards"],$_SESSION["selected.coolers"]);
?>

<script src="js/compatibilty.js"></script>
    <section class="text-gray-600 body-font">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-full mx-auto">
                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Build a system</div>
                    <div class="py-4 px-8 place-items-center">
                        <div class="text-xs md:text-base lg:text-base xl:text-base">
                            <div>
                                <div class="text-xs lg:text-base xl:text-base flex flex-row-reverse border-b-2 border-grey-dark overflow-hidden">
                                    <div class="flex flex-row-reverse rounded mx-1">
                                       <a href="index.php?action=saveBuild<?php if($computerID != null){echo "&computerID=$id";} ?>"><button class="block text-white shadow-border bg-green-500 hover:bg-green-600 py-3 px-4 font-sans tracking-wide uppercase font-bold">
                                            Save PC
                                           </button></a>
                                        <div class="bg-green-400 shadow-border p-0 lg:p-3 xl:p-3">
                                            <div class="w-0 h-0 lg:w-4 lg:h-4 xl:w-4 xl:h-4">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-0 w-0 lg:w-6 xl:h-6" fill="none" viewBox="0 0 24 24" stroke="white">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex flex-row-reverse rounded mx-1">
                                        <button id="pcCompatibility" class="block text-white shadow-border bg-blue-500 hover:bg-blue-700 py-3 px-4 font-sans tracking-wide uppercase font-bold">
                                                Test compatibility
                                            </button>
                                        <div class="bg-blue-600 shadow-border p-0 lg:p-3 xl:p-3">
                                            <div class="w-0 h-0 lg:w-4 lg:h-4 xl:w-4 xl:h-4">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-0 w-0 lg:w-6 xl:h-6" fill="none" viewBox="0 0 24 24" stroke="white">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex flex-row-reverse rounded mx-1">
                                       <a href="index.php?action=exportPdf"> <button class="block text-white shadow-border bg-yellow-500 hover:bg-yellow-600 py-3 px-4 font-sans tracking-wide uppercase font-bold">
                                            Export to PDF
                                           </button></a>
                                        <div class="bg-yellow-400 shadow-border p-0 lg:p-3 xl:p-3">
                                            <div class="w-0 h-0 lg:w-4 lg:h-4 xl:w-4 xl:h-4">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-0 w-0 lg:w-6 xl:h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 10v6m0 0l-3-3m3 3l3-3m2 8H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="container">
                                <table class="w-full flex flex-row flex-no-wrap rounded-lg overflow-hidden sm:shadow-lg my-5">
                                    <thead class="text-white">
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Category</th>
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Price</th>
                                        <th class="p-3 text-left">Actions</th>
                                    </tr>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Category</th>
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Price</th>
                                        <th class="p-3 text-left">Actions</th>
                                    </tr>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Category</th>
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Price</th>
                                        <th class="p-3 text-left">Actions</th>
                                    </tr>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Category</th>
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Price</th>
                                        <th class="p-3 text-left">Actions</th>
                                    </tr>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Category</th>
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Price</th>
                                        <th class="p-3 text-left">Actions</th>
                                    </tr>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Category</th>
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Price</th>
                                        <th class="p-3 text-left">Actions</th>
                                    </tr>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Category</th>
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Price</th>
                                        <th class="p-3 text-left">Actions</th>
                                    </tr>
                                    <tr class="bg-gray-800 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                        <th class="p-3 text-left">Category</th>
                                        <th class="p-3 text-left">Name</th>
                                        <th class="p-3 text-left">Price</th>
                                        <th class="p-3 text-left">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody class="flex-1 sm:flex-none text-white font-bold">
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3">Cases</td>
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.cases"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.cases"][0]["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.cases"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.cases"][0]["name"];} ?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php if($_SESSION["selected.cases"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.cases"][0]["price"]." CHF";} ?></td>
                                        <?php
                                            if(isset($_SESSION["selected.cases"])){
                                                echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=cases">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=cases">Delete</a></td>';
                                            }else{
                                                echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=cases">Select</a></td>';
                                            }
                                        ?>
                                    </tr>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3">Processor</td>
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.processors"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.processors"][0]["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.processors"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.processors"][0]["name"];} ?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php if($_SESSION["selected.processors"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.processors"][0]["price"]." CHF";} ?></td>
                                        <?php
                                        if(isset($_SESSION["selected.processors"])){
                                            echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=processors">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=processors">Delete</a></td>';
                                        }else{
                                            echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=processors">Select</a></td>';
                                        }
                                        ?>
                                    </tr>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3">Cooler</td>
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.coolers"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.coolers"][0]["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.coolers"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.coolers"][0]["name"];} ?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php if($_SESSION["selected.coolers"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.coolers"][0]["price"]." CHF";} ?></td>
                                        <?php
                                        if(isset($_SESSION["selected.coolers"])){
                                            echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=coolers">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=coolers">Delete</a></td>';
                                        }else{
                                            echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=coolers">Select</a></td>';
                                        }
                                        ?>
                                    </tr>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3">Motherboard</td>
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.motherboard"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.motherboard"][0]["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.motherboard"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.motherboard"][0]["name"];} ?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php if($_SESSION["selected.motherboard"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.motherboard"][0]["price"]." CHF";} ?></td>
                                        <?php
                                        if(isset($_SESSION["selected.motherboard"])){
                                            echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=motherboard">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=motherboard">Delete</a></td>';
                                        }else{
                                            echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=motherboard">Select</a></td>';
                                        }
                                        ?>
                                    </tr>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3">Graphics card</td>
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.graphics_cards"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.graphics_cards"][0]["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.graphics_cards"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.graphics_cards"][0]["name"];} ?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php if($_SESSION["selected.graphics_cards"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.graphics_cards"][0]["price"]." CHF";} ?></td>
                                        <?php
                                        if(isset($_SESSION["selected.graphics_cards"])){
                                            echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=graphics_cards">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=graphics_cards">Delete</a></td>';
                                        }else{
                                            echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=graphics_cards">Select</a></td>';
                                        }
                                        ?>
                                    </tr>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3">Memory</td>
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.ram"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.ram"][0]["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.ram"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.ram"][0]["name"];} ?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php if($_SESSION["selected.ram"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.ram"][0]["price"]." CHF";} ?></td>
                                        <?php
                                        if(isset($_SESSION["selected.ram"])){
                                            echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=ram">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=ram">Delete</a></td>';
                                        }else{
                                            echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=ram">Select</a></td>';
                                        }
                                        ?>
                                    </tr>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3">Power Supply</td>
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.power_supply"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.power_supply"][0]["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.power_supply"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.power_supply"][0]["name"];} ?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php if($_SESSION["selected.power_supply"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.power_supply"][0]["price"]." CHF";} ?></td>
                                        <?php
                                        if(isset($_SESSION["selected.power_supply"])){
                                            echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=power_supply">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=power_supply">Delete</a></td>';
                                        }else{
                                            echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=power_supply">Select</a></td>';
                                        }
                                        ?>
                                    </tr>
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="p-3">Storage</td>
                                        <td class="p-3 flex flex-row place-items-center">
                                            <div class="flex-shrink-0 h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30">
                                                <img class="h-0 w-0 lg:h-20 lg:w-20 xl:h-30 xl:w-30 rounded-full" src="<?php if($_SESSION["selected.storage"][0]["image"] == "") { echo "img/defaultImage.jpg";}else{echo $_SESSION["selected.storage"][0]["image"];} ?>" alt="">
                                            </div>
                                            <div class="lg:xl:ml-3 xl:ml-3"><?php if($_SESSION["selected.storage"][0]["name"] == "") { echo "Not selected";}else{echo $_SESSION["selected.storage"][0]["name"];} ?></div>
                                        </td>
                                        <td class="p-3 truncate"><?php if($_SESSION["selected.storage"][0]["price"] == "") { echo "Not Selected";}else{echo $_SESSION["selected.storage"][0]["price"]." CHF";} ?></td>
                                        <?php
                                        if(isset($_SESSION["selected.storage"])){
                                            echo '<td class="p-3"><a class="text-yellow-400 hover:text-yellow-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=storage">Modify</a> <a class="text-red-400 hover:text-red-600 hover:font-medium cursor-pointer" href="index.php?action=deleteSelectedComponent&category=storage">Delete</a></td>';
                                        }else{
                                            echo '<td class="p-3"><a class="text-green-400 hover:text-green-600 hover:font-medium cursor-pointer" href="index.php?action=selectComponent&category=storage">Select</a></td>';
                                        }
                                        ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <style>
                                @media (min-width: 640px) {
                                    table {
                                        display: inline-table !important;
                                    }

                                    thead tr:not(:first-child) {
                                        display: none;
                                    }
                                }

                                td:not(:last-child) {
                                    border-bottom: 0;
                                }

                                th:not(:last-child) {
                                    border-bottom: 0px solid rgba(0, 0, 0, .1);
                                }
                            </style>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
$content = ob_get_clean();
require "userBuilder.php";
?>