/**
 * Author : Nathan Rayburn
 * Date: 01/06/21
 * Description : This file is designed to check the compatibility for the selected components
 **/

$(document).ready(function()
{
    const btn = document.querySelector('#pcCompatibility')
    btn.addEventListener('click',() =>{
        if(checkCompatibility())
        {
            const message = "Your components are compatible."
            alert(message)
        }else{
            const message = "Your components are not compatible."
            alert(message)
        }
    })

})

/**
 * Main function to check compatibility
 */
function checkCompatibility()
{
    const userCookie = getCookie("username"); //get username
    const json = readUserFile(userCookie); //get user's data from temp json file
    var objects = JSON.parse(json); //transform data into objects
    return isCompatible(objects); //return compatibility
}

/**
 * This function is designed to get the cookie from the logged user
 * @param name string
 * return string returns the username
 */
function getCookie(name)
{
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

/**
 * This function is designed to read the user's temp file
 * @param name string
 */
function readUserFile(name)
{
    const target_dir = "temp/";
    const strSep = ".";
    const ext = ".json";
    const temp = "temp";
    const filePath = target_dir + name + strSep + temp + ext;
    //read json file with jquery
    var myjson = { };
    $.ajax({
        url: filePath,
        dataType: 'json',
        async: false,
        success: function(json) {
            myjson = JSON.stringify(json);
        }
    });
    $('#console').append(myjson);

    return myjson;
}

/**
 * This function is designed to check the compatibility between the selected parts
 * @param objects array of objects
 * return truth bool returns true when components are compatible
 */
function isCompatible(objects)
{
    //set local variables
    let truth = true;
    let objectCase = Object;
    let objectProcessor = Object;
    let objectCooler = Object;
    let objectMotherboard = Object;
    let objectGraphicsCard = Object;
    let objectRam = Object;
    let objectStorage = Object;
    let objectPowerSupply = Object;
    //set components from array of objects to local variables
    objects.forEach((element, firstKey) =>{
        element.forEach((component,secondKey)=>{
            if(component.idcases !== "" && component.idcases != null)
            {
                objectCase = component;
            }
            if(component.idprocessors !== "" && component.idprocessors != null)
            {
                objectProcessor = component
            }
            if(component.idcoolers !== "" && component.idcoolers != null)
            {
                objectCooler = component;
            }
            if(component.idmotherboard !== "" && component.idmotherboard != null)
            {
                objectMotherboard = component;
            }
            if(component.idgraphics_cards !== "" && component.idgraphics_cards != null)
            {
                objectGraphicsCard = component;
            }
            if(component.idram !== "" && component.idram != null)
            {
                objectRam = component;
            }
            if(component.idstorage !== "" && component.idstorage != null)
            {
                objectStorage = component
            }
            if(component.idpower_supply !== "" && component.idpower_supply != null)
            {
                objectPowerSupply = component;
            }
        })

    } )
    //check every component is empty
    if(isNotEmpty(objectCase) || isNotEmpty(objectPowerSupply) ||isNotEmpty(objectCooler) || isNotEmpty(objectStorage) ||isNotEmpty(objectProcessor) || isNotEmpty(objectRam) || isNotEmpty(objectGraphicsCard) || isNotEmpty(objectMotherboard))
    {
        //checking compatibility for case and graphics card
        if(isNotEmpty(objectCase) && isNotEmpty(objectGraphicsCard))
        {
            //check if the graphics card fits into the case
            if(!(objectCase.max_length_gpu >= objectGraphicsCard.max_length_gpu))
            {
                truth = false;
            }
        }
        //check if power supply fits case
        if(isNotEmpty(objectCase) && isNotEmpty(objectPowerSupply))
        {
            if(!(objectCase.form_factor_power_supply) === objectPowerSupply.form_factor)
            {
                truth = false;
            }
        }
        //checking compatibility for case and cooler
        if(isNotEmpty(objectCase) && isNotEmpty(objectCooler))
        {
            if(!(parseInt(objectCase.max_height_cpu) >= parseInt(objectCooler.max_height)))
            {
                truth = false;
            }
            if(!(objectCase.aio_water === objectCooler.aio_water))
            {
                truth = false;
            }
        }
        //checking compatibility for case and motherboard
        if(isNotEmpty(objectCase) && isNotEmpty(objectMotherboard))
        {
            //check for form factor
            if(!(objectCase.form_factor_motherboard === objectMotherboard.form_factor))
            {
                truth = false;
            }
        }
        //checking compatibility for motherboard and processor
        if(isNotEmpty(objectMotherboard) && isNotEmpty(objectProcessor))
        {
            //check for socket
            if(!(objectMotherboard.socket_cpu === objectProcessor.socket_cpu))
            {
                truth = false;
            }
        }
        if(isNotEmpty(objectProcessor) && isNotEmpty(objectCooler))
        {
            if(!(objectProcessor.socket_cpu === objectCooler.socket_support))
            {
                truth = false;
            }
        }
        //checking compatibility for ram and motherboard
        if(isNotEmpty(objectMotherboard) && isNotEmpty(objectRam)) {
            //check socket
            if (!(objectMotherboard.form_factor_ram === objectRam.form_factor)) {
                truth = false;
            }
        }

        //checking compatibility for storage
        if(isNotEmpty(objectMotherboard) && isNotEmpty(objectStorage))
        {
            //check m2 interface
            if(!objectStorage.form_factor === "sata")
            {
                if(!(objectMotherboard.interface_m2 === objectStorage.interface_m2)){
                    truth = false;
                }
            }
        }
    }
    return truth;
}

/**
 * This function is designed to check if an object is empty
 * para : obj type object, is a component containing properties
 * return true when the object isn't empty
 */
function isNotEmpty(obj) {
    return Object.keys(obj).length !== 0;
}