<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file will be the index of the whole web-site
 */
session_start();
require 'controller/controller.php';

if(isset($_GET["action"]))
{
    switch($_GET["action"])
    {
        case "editComponent":
            editComponent($_GET["category"],$_GET["id"]);
            break;
        case "visibilityComponent":
            visibilityComponent($_GET["category"],$_GET["id"]);
            break;
        case "deleteSelectedBuild" :
            deleteBuild($_GET["computerType"],$_GET["computerID"]);
            break;
        case "mybuilds":
            myBuilds();
            break;
        case "exportPdf":
            exportPdf();
            break;
        case "saveBuild":
            saveBuild($_GET["computerType"]);
            break;
        case "deleteSelectedComponent" :
            deleteSelectedComponent($_GET["category"],$_GET["id"]);
            breaK;
        case "addSelectedComponent":
            addSelectedComponent($_GET["category"],$_GET["id"]);
            break;
        case "selectComponent":
            selectComponent($_GET["category"]);
            break;
        case "createComponent":
            createComponent($_POST,$_GET["category"],$_GET["id"]);
            break;
        case "componentList":
            componentList($_GET["category"]);
            break;
        case "userBuildMenu":
            userBuildMenu();
            break;
        case "userCreateBuild":
            createBuild($_GET["computerType"],$_GET["computerID"]);
            break;
        case "deleteUser":
            deleteUser($_GET["username"]);
            break;
        case "editUser":
            editUser($_POST,$_GET["username"]);
            break;
        case "createNewUser":
            createNewUser($_POST);
            break;
        case "userPassword":
            userPassword($_POST);
            break;
        case "profileInformation":
            profileInformation($_POST);
            break;
        case "profile" :
            profile();
            break;
        case "adminInventory" :
            inventoryManager();
            break;
        case "adminModel":
            modelManager();
            break;
        case "admin":
            admin();
            break;
        case "adminUser" :
            userManager();
            break;
        case "home":
            home();
            break;
        case "login":
            login($_POST);
            break;
        case "logout":
            logout();
            break;
        case "register":
            register($_POST);
            break;
        default:
            if(isset($_SESSION["userEmail"]))
            {
                home();
            }else{
                login($_POST);
            }
            break;
    }
}
else
{
    home();
}
